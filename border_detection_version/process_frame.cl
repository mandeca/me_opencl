typedef unsigned char byte;
typedef char s8;
typedef unsigned short u16;
typedef short s16;

#define REF_WIDTH 46

u16 SAD_macroblock(local byte * restrict search_area, local byte * restrict macroblock)
{
	u16 sad = 0;

	#pragma unroll
	for (s16 _ = 0; _ < 256; ++_)
	{
		s8 i = _ / 16, j = _ % 16;
		sad += abs(search_area[i * REF_WIDTH + j] - macroblock[_]);
	}

	return sad;
}

void load_macroblock(local byte * restrict out, global byte * restrict in,
	u16 x, u16 y, u16 lx)
{
	in += (y * lx) + x;

	for (s16 _ = 0; _ < 256; ++_)
	{
		s8 i = _ / 16, j = _ % 16;
		out[_] = in[i * lx + j];
	}
}

void load_ref_block(local byte * restrict out, global byte * restrict in,
	u16 x, u16 y, s8 sx, s8 sy, 
	s8 x_size, s8 y_size, u16 lx)
{
	in += (y * lx) + x;
	out += sy * REF_WIDTH + sx;

	for (s16 _ = 0; _ < y_size * x_size; ++_)
	{
		s8 i = _ / x_size, j = _ % x_size;
		out[i * REF_WIDTH + j] = in[i * lx + j];
	}
}

void motion_estimation(global byte * restrict curr, global byte * restrict prev,
	local byte * restrict macroblock, local byte * restrict ref_block,
	global u16 * restrict SAD,
	global s8 * restrict vector_x, global s8 * restrict vector_y,
	u16 x_curr, u16 y_curr, s8 dx_min, s8 dx_max, s8 dy_min, s8 dy_max,
	u16 pels)
{
	local byte *search_area = ref_block + 15 * REF_WIDTH + 15; // Displacement to center the area with the macroblock
	u16 sad, best_sad = USHRT_MAX;
	s8 vx, vy; // Horizontal and vertical displacements

	for (s8 i = dy_min; i <= dy_max; ++i)
	{
		for (s8 j = dx_min; j <= dx_max; ++j)
		{
			s16 starting_point = i * REF_WIDTH + j; // Displacement to start the SAD
			// (Instead of using starting_point we could also modify search_area directly.)
			sad = SAD_macroblock(search_area + starting_point, macroblock);

			if (sad < best_sad)
			{
				best_sad = sad; // We take the best (closest) match
				vx = j;
				vy = i;
			}
		}
	}

	*SAD = best_sad;
	*vector_x = vx;
	*vector_y = vy;
}

__attribute__((max_global_work_dim(0)))
kernel void process_frame(global byte * restrict curr, global byte * restrict prev,
	__attribute__((local_mem_size(256))) local byte * restrict macroblock,
	__attribute__((local_mem_size(4096))) local byte * restrict ref_block,
	global u16 * restrict SADs,
	global s8 * restrict x_vectors, global s8 * restrict y_vectors)
{
	s8 dy_min, dy_max, dx_min, dx_max;
	u16 _ = 0;

	for (u16 i = 0; i < 1080; i += 16)
	{
		// dy_min
		if (i == 0)
			dy_min = 0;
		else
			dy_min = -15;

		// dy_max
		if (i == 1088 - 16)
			dy_max = 0;
		else
			dy_max = 15;

		for (u16 j = 0; j < 1920; j += 16)
		{
			// dx_min
			if (j == 0)
				dx_min = 0;
			else
				dx_min = -15;

			// dx_max
			if (j == 1920 - 16)
				dx_max = 0;
			else
				dx_max = 15;

			//#pragma loop_fuse independent
			{
			// Load area macroblock
			load_macroblock(macroblock, curr, j, i, 1920);

			// Load area ref_block
			// ref_block is 46 x 46; usually all its elements are used but less might be used when
			// prev is a border or corner of the original frame.
			load_ref_block(ref_block, prev,
				j + dx_min, i + dy_min, // Pixel from prev from where to start to copy
				15 + dx_min, 15 + dy_min, // Pixel on which to place the data on ref_block
				16 + dx_max - dx_min, 16 + dy_max - dy_min, // Size to copy to ref_block
				1920);
			// NOTE: dx_min and dx_max are integers in [-15, 15]. Used as failsafe for when
			// not copying the full 46 x 46 pixels.
			}

			motion_estimation(curr, prev, macroblock, ref_block,
				&SADs[_], &x_vectors[_], &y_vectors[_],
				j, i, dx_min, dx_max, dy_min, dy_max, 1920);
			_++;
		}
	}
}
