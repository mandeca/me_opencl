typedef unsigned char byte;
typedef char s8;
typedef unsigned short u16;
typedef short s16;

#define REF_WIDTH 46

u16 SAD_macroblock(local byte * restrict search_area, local byte * restrict macroblock)
{
	u16 sad = 0;

	#pragma unroll
	for (s16 _ = 0; _ < 256; ++_)
	{
		s8 i = _ / 16, j = _ % 16;
		sad += abs(search_area[i * REF_WIDTH + j] - macroblock[_]);
	}

	return sad;
}

void load_frame(local byte out[(1920 + 30) * (1088 + 30)], global byte * restrict in)
{
	for (int i = 0; i < (1920 + 30) * (1088 + 30); ++i)
		 out[i] = in[i];
}


void load_macroblock(local byte * restrict out, local byte in[(1920 + 30) * (1088 + 30)],
	u16 x, u16 y, u16 lx)
{
	in += (y * lx) + x;

	for (s16 _ = 0; _ < 256; ++_)
	{
		s8 i = _ / 16, j = _ % 16;
		out[_] = in[i * lx + j];
	}
}

void load_ref_block(local byte * restrict out, local byte in[(1920 + 30) * (1088 + 30)],
	u16 x, u16 y, u16 lx)
{
	in += (y * lx) + x;

	for (s16 _ = 0; _ < 46 * REF_WIDTH; ++_)
	{
		s8 i = _ / REF_WIDTH, j = _ % REF_WIDTH;
		out[i * REF_WIDTH + j] = in[i * lx + j];
	}
}

void motion_estimation(local byte * restrict macroblock, local byte * restrict ref_block,
	global u16 * restrict SAD,
	global s8 * restrict vector_x, global s8 * restrict vector_y)
{
	local byte *search_area = ref_block;
	u16 sad, best_sad = USHRT_MAX;
	s8 vx, vy; // Horizontal and vertical displacements

	for (s8 i = -15; i <= 15; ++i)
	{
		for (s8 j = -15; j <= 15; ++j)
		{
			sad = SAD_macroblock(search_area++, macroblock);

			if (sad < best_sad)
			{
				best_sad = sad; // We take the best (closest) match
				vx = j;
				vy = i;
			}
		}
		search_area += REF_WIDTH - 31;
	}

	*SAD = best_sad;
	*vector_x = vx;
	*vector_y = vy;
}

__attribute__((max_global_work_dim(0)))
kernel void process_frame(global byte * restrict curr, global byte * restrict prev,
	__attribute__((local_mem_size(256))) local byte * restrict macroblock,
	__attribute__((local_mem_size(4096))) local byte * restrict ref_block,
	global u16 * restrict SADs,
	global s8 * restrict x_vectors, global s8 * restrict y_vectors)
{
	local byte cur_frame[(1920 + 30) * (1088 + 30)];
	local byte ref_frame[(1920 + 30) * (1088 + 30)];

	load_frame(cur_frame, curr);
	load_frame(ref_frame, prev);

	u16 _ = 0;

	for (u16 i = 15; i < 1088 + 15; i += 16)
	{
		for (u16 j = 15; j < 1920 + 15; j += 16)
		{

			//#pragma loop_fuse independent
			{
			// Load area macroblock
			load_macroblock(macroblock, cur_frame, j, i, 1920 + 30);

			// Load area ref_block
			load_ref_block(ref_block, ref_frame,
				j - 15, i - 15, // Pixel from prev from where to start to copy
				1920 + 30);
			}

			motion_estimation(macroblock, ref_block,
				&SADs[_], &x_vectors[_], &y_vectors[_]);
			_++;
		}
	}
}
