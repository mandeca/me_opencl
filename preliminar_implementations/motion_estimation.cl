typedef unsigned char byte;
typedef char s8; // To save some resources on loop indexes and offsets
typedef unsigned short u16;
typedef short s16;

//#define macroblock_IN_REGISTERS

#define REF_WIDTH 46

u16 SAD_macroblock(local byte * restrict search_area, local byte * restrict macroblock_ram)
{
	#ifdef macroblock_IN_REGISTERS
	byte macroblock[256];
	load_macroblock_to_regs:
	#pragma unroll
	for (byte i = 0; i < 256; ++i)
		 macroblock[i] = macroblock_ram[i];
	#else // !macroblock_IN_REGISTERS
	#define macroblock macroblock_ram
	#endif // macroblock_IN_REGISTERS

	u16 sad = 0;

	SAD_loop:
	for (s8 _ = 0; _ < 16; ++_)
	{
		#pragma unroll
		for (s8 i = 0; i < 16; ++i)
			sad += abs(search_area[i] - macroblock[i]);

		search_area += REF_WIDTH;
		macroblock += 16;
	}

	return sad;
}

void load_macroblock(local byte * restrict out, global byte * restrict in,
	u16 x, u16 y, u16 lx)
{
	in += (y * lx) + x;

	macroblock_loading:
	#pragma loop_coalesce
	for (s8 i = 0; i < 16; ++i)
	{
		for (s8 j = 0; j < 16; ++j)
			*out++ = *in++;
		in += lx - 16;
	}
}

void load_ref_block(local byte * restrict out, global byte * restrict in,
	u16 x, u16 y, s8 sx, s8 sy, 
	s8 x_size, s8 y_size, u16 lx)
{
	for (u16 k = 0; k < 46 * REF_WIDTH; ++k) out[k] = 0;

	in += (y * lx) + x;
	out += sy * REF_WIDTH + sx;

	ref_block_loading:
	#pragma loop_coalesce
	for (s8 i = 0; i < y_size; ++i)
	{
		for (s8 j = 0; j < x_size; ++j)
			*out++ = *in++;
		in += lx - x_size;
		out += REF_WIDTH - x_size;
	}
}

__attribute__((max_global_work_dim(0)))
kernel void motion_estimation(global byte * restrict curr, global byte * restrict prev,
	__attribute__((local_mem_size(256))) local byte * restrict macroblock,
	__attribute__((local_mem_size(4096))) local byte * restrict ref_block,
	global u16 * restrict SAD,
	global s8 * restrict vector_x, global s8 * restrict vector_y,
	u16 x_curr, u16 y_curr, s8 dx_min, s8 dx_max, s8 dy_min, s8 dy_max,
	u16 pels)
{
	#pragma loop_fuse independent
	{
	// Load area macroblock
	load_macroblock(macroblock, curr, x_curr, y_curr, pels);

	// Load area ref_block
	// ref_block is 46 x 46; usually all its elements are used but less might be used when
	// prev is a border or corner of the original frame.
	load_ref_block(ref_block, prev,
		x_curr + dx_min, y_curr + dy_min, // Pixel from prev from where to start to copy
		15 + dx_min, 15 + dy_min, // Pixel on which to place the data on ref_block
		16 + dx_max - dx_min, 16 + dy_max - dy_min, // Size to copy to ref_block
		pels);
	// NOTE: dx_min and dx_max are integers in [-15, 15]. Used as failsafe for when
	// not copying the full 46 x 46 pixels.
	}

	local byte *search_area = ref_block + 15 * REF_WIDTH + 15; // Displacement to center the area with the macroblock
	u16 sad, best_sad = USHRT_MAX;
	s8 vx, vy; // Horizontal and vertical displacements

	best_SAD_search:
	for (s8 i = dy_min; i <= dy_max; ++i)
	{
		for (s8 j = dx_min; j <= dx_max; ++j)
		{
			s16 starting_point = i * REF_WIDTH + j; // Displacement to start the SAD
			// (Instead of using starting_point we could also modify search_area directly.)
			sad = SAD_macroblock(search_area + starting_point, macroblock);

			if (sad < best_sad)
			{
				best_sad = sad; // We take the best (closest) match
				vx = j;
				vy = i;
			}
		}
	}

	*SAD = best_sad;
	*vector_x = vx;
	*vector_y = vy;
}
