typedef unsigned char byte;

__kernel void SAD_macroblock(global byte *restrict search_area, global byte *restrict act_block, global int *restrict SAD_out, int h_length, int min_FRAME)
{
	int sad = 0;

	#pragma unroll
	for (int _ = 0; _ < 16; _++)
	{
		#pragma unroll
		for (int i = 0; i < 16; i++)
			sad += abs(search_area[i] - act_block[i]);

		search_area += h_length;
		act_block += 16;
		if (sad > min_FRAME)
		{
			*SAD_out = INT_MAX;
			return;
		}
	}
	*SAD_out = sad;
}

