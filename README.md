# Codes developed for the work "Implementation of a motioon estimation algorithm for Intel FPGAs using OpenCL

This repository contains a set of codes developed for the work [Implementation of a motion estimation algorithm for Intel FPGAs using OpenCL](https://link.springer.com/article/10.1007/s11227-023-05051-3).

# Repository structure

The directory [boder_detection_version](./border_detection_version) includes the codes for the **implementation of block-matching motion estimaton that adds logic to detect frame borders**.
 - `ME_FPGA_test_fhd.c` is the host code.
 - `process_frame.cl` is the full search block-matching motion estimation OpenCL kernel.
 - `make.sh` is the command-line command to compile the host code on Intel DevCloud.
 - `compile.sh` is a bash script to compiple the OpenCL kernel on Intel DevCloud.
 - `output.txt` is an example of the stdout output of the execution of this implementation.

The directory [extended_frames_version](./extended_frames_version) includes the code for the **implementation of block-matching motion estimation that works on frames with extended borders**, as well as the **preliminar implementation that uses a systolic array**, and the **preliminar implementation that uses diamond search**.
 - `ME_FPGA_test_fhd.c` is the host code.
 - `process_frame.cl` is the full search block-matching motion estimation OpenCL kernel.
 - `process_frame_systolic_array.cl` is the preliminar full search block-matching motion estimation OpenCL kernel that uses a systolic array.
 - `process_frame_diamond.cl` is the preliminar diamnd search block-matching motion estimation OpenCL kernel.
 - `make.sh` is the command-line command to compile the host code on Intel DevCloud.
 - `compile.sh` is a bash script to compiple the OpenCL kernel on Intel DevCloud.
 - `output.txt` is an example of the stdout output of the execution of this implementation.

The directory [references](./references) includes the **developed reference codes to make theoretical and experimental comparisons** with the OpenCL implementations developed.
 - `ref_cpu.c` includes all the CPU versions developed (no vectorization, using MMX intrinsics, and using SSE intrinsics). The different versions are compiled through conditional compilation.
 - `ref_hls_systolic_array.cpp` is a Xilinx Vitis HLS code that implement full search block-matching motion estimation using a systolic array. This implementation is not referenced in the manuscript, but was used as a baseline and for debugging the developed VHDL implementation.
 - `ref_hdl_registro_desplazamiento.vhd` and `redux6.vhd` are the VHDL codes that implement full search block-matching motion estimation using a systolic array.

The directory [preliminar_implementations](./preliminar_implementations) contains earier versions of the developed OpenCL kernels, which provide insight into the development process followed.

The directory [compilation_reports](./compilation_reports) includes compilation reports for some (not all) of the kernels developed during this work. Altogether, these reports show the evolution of the developed solution over time. The included reports are:
 - (v1.0) The first kernel that succesfully compiled.
 - (v4.0) The first kernel used for execution tests of performance and correctness.
 - (v5.1) One of the first kernels to perform full search over an entire macroblock.
 - (v6.0) The first kernel that processed an entire frame inside the FPGA.
 - (v6.1) The final implementation of full search block-matching motion estimation that adds border detection logic.
 - (v7.0) The final implementation of full search block-matching motion estimation that works on frames with extended borders.
 - (v9.0) The preliminar implementation of diamond search block-matching motion estimation.

The compilation reports are designed to be visualized using and internet browser and not as plain text.

# Citation

If you use this software or do research based on it, please cite it as below.

 * [Implementation of a motion estimation algorithm for Intel FPGAs using OpenCL](https://link.springer.com/article/10.1007/s11227-023-05051-3)
```BibTeX
@article{deCastro2023:MotionEstimation,
    author = {de Castro, Manuel and Osorio, Roberto R. and Vilariño, David L. and Gonzalez-Escribano, Arturo and Llanos, Diego R.},
    year = {2023},
    month = {01},
    title = {Implementation of a motion estimation algorithm for Intel FPGAs using OpenCL},
    journal = {The Journal of Supercomputing},
    issn={1573-0484},
    doi = {10.1007/s11227-023-05051-3}
}
```
