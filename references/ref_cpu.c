#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include <omp.h>

//#define conMMX
//#define conSSE
//#define conOMP

#ifdef conSSE 
    #include <emmintrin.h>
    #define CUAR 48 
#else
    #ifdef conMMX
        #include<xmmintrin.h>
        #define CUAR 48 
    #else
        #define CUAR 46
    #endif
#endif

static inline double wtime(void)
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec + 1.0e-6 * tv.tv_usec;
}

#ifdef conSSE 
int SAD_Macroblock(unsigned char* ii, unsigned char* act_block)
{

    __m128i* curP, *prevP;
    __m128i xmm1, xmm2, xmm3, xmm4, xmm5;
    int i;
    int SAD = 0;

    _mm_empty();

    curP = (__m128i*) act_block;
    prevP = (__m128i*) ii;

    xmm3 = _mm_setzero_si128();
       
    for (i = 0; i < 16; ++i) {
        xmm1 = _mm_loadu_si128(curP);
        //xmm1 = *curP++;     
        xmm2 = _mm_loadu_si128(prevP);
   
        xmm1 = _mm_sad_epu8(xmm1, xmm2);
        xmm3 = _mm_add_epi32(xmm3, xmm1);   // 32 bits estar� bien

        curP++;
        prevP += 3; // 48/16=3
    }
   
    SAD = _mm_extract_epi16(xmm3, 0) + _mm_extract_epi16(xmm3, 4);

    _mm_empty(); 

    return SAD;

}
#else

#ifdef conMMX
int SAD_Macroblock(unsigned char* ii, unsigned char* act_block)
{

    __m64* curP, * prevP;
    __m64 mmx1, mmx2, mmx3, mmx4, mmx5;
    int i;
    int SAD = 0;

    _mm_empty(); //

    curP = (__m64*) act_block;
    prevP = (__m64*) ii;

    mmx5 = _mm_setzero_si64();

    for (i = 0; i < 16; ++i) {
        mmx1 = *curP++;		mmx2 = *curP++; // importante incrementar para pasar a la siguiente l�nea
        mmx3 = *prevP++;	mmx4 = *prevP;
        mmx1 = _m_psadbw(mmx1, mmx3);
        mmx2 = _m_psadbw(mmx2, mmx4);
        mmx5 = _m_paddw(mmx5, mmx1);
        mmx5 = _m_paddw(mmx5, mmx2);
        //curP +=  ...nada, al terminar una fila comienza la siguiente
        prevP += 5; // 48/8=6, y s�lo hemos avanzado preVP 1 vez
    }
    SAD = (int)mmx5[0];//.m64_i32[0];
    _mm_empty(); //

    return SAD;

}
#else

int SAD_Macroblock(unsigned char* ii, unsigned char* act_block)
{
    int i, j;
    int abso, sad = 0;
    unsigned char* kk;

    kk = act_block;
    #ifdef conOMP
    #pragma omp parallel for schedule(static)
    #endif
    for (i = 0; i < 16; ++i) {
        for (j = 0; j < 16; ++j) {
            abso = abs((*ii++) - (*kk++));
            sad += abso; 
        }
        ii += CUAR-16; // siempre mide CUAR-16 bytes de ancho
    }

    return sad;
}
#endif 
#endif

void LoadActBlock(unsigned char* im, unsigned char* block, int x, int y, int lx)
{
    unsigned char* in, *out;
    int i, j; 

    in = im + (y * lx) + x;     // me pregunto si para OpenCL ser� m�s sencillo usar arrays o usar punteros
    out = block;

    for(i=0; i<16; ++i){
        for (j = 0; j < 16; ++j) 
            *out++ = *in++;
        in += lx - 16;
    }
}


void LoadRefBlock(unsigned char* im, unsigned char* block, int x, int y, int lx)
    /* 
    im = puntero a la imagen
    block = puntero al bloque 46xCUAR
    (x, y) = posici�n de la esquina superior izquierda en la imagen completa
    lx = anchura de la imagen completa
    */
{
    unsigned char* in;
    unsigned char* out;
    int i, j; 

    in = im + (y * lx) + x;
    out = block;

    for(i=0; i< 46; ++i) {
        for(j=0; j<CUAR; ++j)
            *out++ = *in++;        
        in += lx - CUAR;
    };

}


int MotionEstimation(unsigned char* act_block, unsigned char* ref_block)
{
    int i, j; 
    int SAD, best, vx=0, vy=0;
    unsigned char* search_area;

    best = 10000000;
    search_area = ref_block;
    for (i = -15; i <= 15; ++i) {
        for (j = -15; j <= 15; ++j) {
            SAD = SAD_Macroblock(search_area++, act_block);
            if (SAD < best) {
                best = SAD;
                vx = j; 
                vy = i; 
            }            
        }      
        search_area += CUAR - 31; 
    }

    return best;
}


int main(int nargs, char* vargs[]) {

    FILE* fp;
    unsigned char *act, *ref;
    unsigned char* act_block, * ref_block;
    unsigned char* ACTU, *REFE, *tmp;
    int i, j, REP;
    long acum = 0; 
    double time1 = 0, time2; 

    if (nargs != 3)
        exit(-1);
   
    fp = fopen(vargs[1], "rb");     // hay que suministrar el nombre de un archivo con 2 frames
    sscanf(vargs[2], "%d", &REP);

    //esta version necesita que sean de tama�o extendido: (30 + 1920) * (30 + 1088), s�, no es de 1080


    act = malloc((1920 + 30) * (1088 + 30));
    ref = malloc((1920 + 30) * (1088 + 30));       // no me molesto en comprobar los punteros

    fread(act, 1920 + 30, 1088 + 30, fp);       // dimensiones extendidas
    fread(ref, 1920 + 30, 1088 + 30, fp);

    fclose(fp);

    act_block = malloc(16 * 16);     // arrays en memoria local interna
    ref_block = malloc(46 * CUAR);     // no compruebo que se hayan creado correctamente


    ACTU = act;
    REFE = ref;

    int _REP = REP + 100;
    while (_REP--) {

    	if (_REP == REP) time1 = wtime(); 

        for (i = 15; i < (1088 + 15); i += 16) {

            for (j = 15; j < (1920 + 15); j += 16) {

                LoadActBlock(ACTU, act_block, j, i, 1920+30);
                LoadRefBlock(REFE, ref_block, j-15, i-15, 1920+30);

                int me = MotionEstimation(act_block, ref_block);
		acum += me;
            }
        }

        tmp = REFE;
        REFE = ACTU;
        ACTU = tmp;

    }

    time2 = wtime();

    printf("Total checksum: %ld\n", acum);
    printf("Execution time: %lf   \n", time2 - time1);


    free(act);
    free(ref);

    return acum; 

  // NUEVO EXTENDIDO
  // para 1 repeticion acum = 18292643
  // para 2 repeticiones acum = 36431066
  // para 10 repeticiones acum = 182155330, que es 5 x 36431066
}
