#include <iostream>
#include "ap_int.h"
using namespace std;




ap_uint<16> me(ap_uint<2> shift, ap_uint<32> data){

/*#pragma HLS INTERFACE ap_ctrl_none port=shift
#pragma HLS INTERFACE ap_none port=shift
#pragma HLS INTERFACE ap_ctrl_none port=data
#pragma HLS INTERFACE ap_none port=data
	*/
#pragma HLS pipeline



	static ap_uint<8> act[256];
	static ap_uint<8> ref[46*48]; 	// 2208
	ap_uint<9> dif[256]; 				// lo hago sin sign porque lo cocino todo yo
	ap_uint<8> abso[256];
	ap_uint<256> signos;
	ap_uint<3> packSignos[43];
	const ap_uint<3> PACKING[64]={0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6};
	ap_uint<9> todosLosSignos;
	ap_uint<16> todosLosAbs;

#pragma HLS ARRAY_PARTITION variable = act complete dim=0
//#pragma HLS ARRAY_PARTITION act complete dim=2

#pragma HLS ARRAY_PARTITION variable = ref complete dim=0
//#pragma HLS ARRAY_PARTITION ref complete dim=2


	for(int i=0; i<16; ++i){
		#pragma HLS unroll
		for(int j=0; j<16; ++j){
		#pragma HLS unroll
			dif[i*16+j] = ((ap_uint<9>)act[i*16+j]) - ((ap_uint<9>)ref[i*48+j+(30*48)+32]);
//				cout << hex << dif[i*16+j] << " = " << act[i*16+j] << " - " << ref[i*48+j+(30*48)+32] << endl;
		}
//			cout << endl;
	}

	for(int i=0; i<256; ++i){
		#pragma HLS unroll
		abso[i] = dif[i].bit(8) ? ~dif[i](7, 0) : dif[i](7, 0); 	// esta "resta" se hace con C'1
		signos.bit(i) = dif[i].bit(8);
//		if(dif[i].bit(8)) cout << hex << dif[i] << " -> " << abso[i] << endl;
//		cout << abso[i] << endl;
	}

	//cout << hex << signos << endl;



	for(int i=0; i<42; ++i){			// contamos cuantas restas dieron negativo, y eso convertir� loa C'1 an C'2
		#pragma HLS unroll
		packSignos[i]=PACKING[signos(i*6, i*6+5)];
	}
	packSignos[42] = PACKING[signos(255, 252)];

	todosLosSignos = 0;
	for(int i=0; i<43; ++i){
		#pragma HLS unroll
		todosLosSignos +=  packSignos[i];
	}

//		cout << "SIGNOS: " << todosLosSignos << endl;

	todosLosAbs = 0;
	for(int i=0; i<256; ++i){
		#pragma HLS unroll
		todosLosAbs +=  abso[i];
	}



	switch(shift){
		case 0: break;
		case 1: // data input
			for(int i=255; i>=4; --i)
				#pragma HLS unroll
				act[i] = act[i-4];
			act[3] = ref[2207];	act[2] = ref[2207-1];	act[1] = ref[2207-2];	act[0] = ref[2207-3];
			for(int i=2207; i>=4; --i)
				#pragma HLS unroll
				ref[i] = ref[i-4];
			ref[3] = data(31, 24); 	ref[2] = data(23, 16); 	ref[1] = data(15, 8); 	ref[0] = data(7, 0);
			break;
		case 2: // shift 1
			for(int i=2207; i>=1; --i)
				#pragma HLS unroll
				ref[i] = ref[i-1];
			break;
		default: // shift 18 (cambio de l�nea)
			for(int i=2207; i>=18; --i)
				#pragma HLS unroll
				ref[i] = ref[i-18];
			break;
	};

	return todosLosAbs + todosLosSignos;

}
