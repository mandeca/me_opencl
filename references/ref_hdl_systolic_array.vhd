library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;


-- el array REF es de 46 x 48 p�xeles. Podr�asn ser 46x46, pero 
-- me complica un poco la carga
-- adem�s, al poner 48 es m�s f�cil hacer despu�s la versi�n que reutiliza datos ya cargados


entity ME is
    Port ( clk : in STD_LOGIC;
           --validIn : in STD_LOGIC;
           dataIn : in std_logic_vector(31 downto 0);
           shift : in std_logic_vector(1 downto 0);
           SAD : out std_logic_vector(15 downto 0));
end ME;

architecture Behavioral of ME is

signal act : std_logic_vector(2047 downto 0);
signal ref : std_logic_vector(17663 downto 0);

signal resta0, resta1, resta2, resta3, resta4, resta5, resta6, resta7, resta8, resta9, resta10, resta11, resta12, resta13, resta14, resta15, resta16, resta17, resta18, resta19, resta20, resta21, resta22, resta23, resta24, resta25, resta26, resta27, resta28, resta29, resta30, resta31, resta32, resta33, resta34, resta35, resta36, resta37, resta38, resta39, resta40, resta41, resta42, resta43, resta44, resta45, resta46, resta47, resta48, resta49, resta50, resta51, resta52, resta53, resta54, resta55, resta56, resta57, resta58, resta59, resta60, resta61, resta62, resta63, resta64, resta65, resta66, resta67, resta68, resta69, resta70, resta71, resta72, resta73, resta74, resta75, resta76, resta77, resta78, resta79, resta80, resta81, resta82, resta83, resta84, resta85, resta86, resta87, resta88, resta89, resta90, resta91, resta92, resta93, resta94, resta95, resta96, resta97, resta98, resta99, resta100, resta101, resta102, resta103, resta104, resta105, resta106, resta107, resta108, resta109, resta110, resta111, resta112, resta113, resta114, resta115, resta116, resta117, resta118, resta119, resta120, resta121, resta122, resta123, resta124, resta125, resta126, resta127, resta128, resta129, resta130, resta131, resta132, resta133, resta134, resta135, resta136, resta137, resta138, resta139, resta140, resta141, resta142, resta143, resta144, resta145, resta146, resta147, resta148, resta149, resta150, resta151, resta152, resta153, resta154, resta155, resta156, resta157, resta158, resta159, resta160, resta161, resta162, resta163, resta164, resta165, resta166, resta167, resta168, resta169, resta170, resta171, resta172, resta173, resta174, resta175, resta176, resta177, resta178, resta179, resta180, resta181, resta182, resta183, resta184, resta185, resta186, resta187, resta188, resta189, resta190, resta191, resta192, resta193, resta194, resta195, resta196, resta197, resta198, resta199, resta200, resta201, resta202, resta203, resta204, resta205, resta206, resta207, resta208, resta209, resta210, resta211, resta212, resta213, resta214, resta215, resta216, resta217, resta218, resta219, resta220, resta221, resta222, resta223, resta224, resta225, resta226, resta227, resta228, resta229, resta230, resta231, resta232, resta233, resta234, resta235, resta236, resta237, resta238, resta239, resta240, resta241, resta242, resta243, resta244, resta245, resta246, resta247, resta248, resta249, resta250, resta251, resta252, resta253, resta254, resta255 : std_logic_vector(8 downto 0);
signal corr0, corr1, corr2, corr3, corr4, corr5, corr6, corr7, corr8, corr9, corr10, corr11, corr12, corr13, corr14, corr15, corr16, corr17, corr18, corr19, corr20, corr21, corr22, corr23, corr24, corr25, corr26, corr27, corr28, corr29, corr30, corr31, corr32, corr33, corr34, corr35, corr36, corr37, corr38, corr39, corr40, corr41, corr42, corr43, corr44, corr45, corr46, corr47, corr48, corr49, corr50, corr51, corr52, corr53, corr54, corr55, corr56, corr57, corr58, corr59, corr60, corr61, corr62, corr63, corr64, corr65, corr66, corr67, corr68, corr69, corr70, corr71, corr72, corr73, corr74, corr75, corr76, corr77, corr78, corr79, corr80, corr81, corr82, corr83, corr84, corr85, corr86, corr87, corr88, corr89, corr90, corr91, corr92, corr93, corr94, corr95, corr96, corr97, corr98, corr99, corr100, corr101, corr102, corr103, corr104, corr105, corr106, corr107, corr108, corr109, corr110, corr111, corr112, corr113, corr114, corr115, corr116, corr117, corr118, corr119, corr120, corr121, corr122, corr123, corr124, corr125, corr126, corr127, corr128, corr129, corr130, corr131, corr132, corr133, corr134, corr135, corr136, corr137, corr138, corr139, corr140, corr141, corr142, corr143, corr144, corr145, corr146, corr147, corr148, corr149, corr150, corr151, corr152, corr153, corr154, corr155, corr156, corr157, corr158, corr159, corr160, corr161, corr162, corr163, corr164, corr165, corr166, corr167, corr168, corr169, corr170, corr171, corr172, corr173, corr174, corr175, corr176, corr177, corr178, corr179, corr180, corr181, corr182, corr183, corr184, corr185, corr186, corr187, corr188, corr189, corr190, corr191, corr192, corr193, corr194, corr195, corr196, corr197, corr198, corr199, corr200, corr201, corr202, corr203, corr204, corr205, corr206, corr207, corr208, corr209, corr210, corr211, corr212, corr213, corr214, corr215, corr216, corr217, corr218, corr219, corr220, corr221, corr222, corr223, corr224, corr225, corr226, corr227, corr228, corr229, corr230, corr231, corr232, corr233, corr234, corr235, corr236, corr237, corr238, corr239, corr240, corr241, corr242, corr243, corr244, corr245, corr246, corr247, corr248, corr249, corr250, corr251, corr252, corr253, corr254, corr255 : std_logic_vector(7 downto 0);
signal one0, one1, one2, one3, one4, one5, one6, one7, one8, one9, one10, one11, one12, one13, one14, one15, one16, one17, one18, one19, one20, one21, one22, one23, one24, one25, one26, one27, one28, one29, one30, one31, one32, one33, one34, one35, one36, one37, one38, one39, one40, one41, one42, one43, one44, one45, one46, one47, one48, one49, one50, one51, one52, one53, one54, one55, one56, one57, one58, one59, one60, one61, one62, one63 : std_logic_vector(9 downto 0);
signal two0, two1, two2, two3, two4, two5, two6, two7, two8, two9, two10, two11, two12, two13, two14, two15 : std_logic_vector(11 downto 0);
signal three0, three1, three2, three3 : std_logic_vector(13 downto 0);

signal signos00, signos01, signos02, signos03, signos04, signos05, signos06, signos07, signos08, signos09, signos10, signos11, signos12, signos13, signos14, signos15, signos16, signos17, signos18, signos19, signos20, signos21, signos22, signos23, signos24, signos25, signos26, signos27, signos28, signos29, signos30, signos31, signos32, signos33, signos34, signos35, signos36, signos37, signos38, signos39, signos40, signos41, signos42 : std_logic_vector(2 downto 0); 
signal regSignos00, regSignos01, regSignos02, regSignos03, regSignos04, regSignos05, regSignos06, regSignos07, regSignos08, regSignos09, regSignos10, regSignos11, regSignos12, regSignos13, regSignos14, regSignos15, regSignos16, regSignos17, regSignos18, regSignos19, regSignos20, regSignos21, regSignos22, regSignos23, regSignos24, regSignos25, regSignos26, regSignos27, regSignos28, regSignos29, regSignos30, regSignos31, regSignos32, regSignos33, regSignos34, regSignos35, regSignos36, regSignos37, regSignos38, regSignos39, regSignos40, regSignos41, regSignos42 : std_logic_vector(2 downto 0);

signal sigOne0, sigOne1, sigOne2, sigOne3, sigOne4, sigOne5, sigOne6, sigOne7, sigOne8, sigOne9, sigOne10 : std_logic_vector(4 downto 0);
signal sigTwo0, sigTwo1, sigTwo2 : std_logic_vector(6 downto 0);
signal sigThree : std_logic_vector(8 downto 0);


signal chivato : std_logic_vector(191 downto 0);
signal ultimos : std_logic_vector(95 downto 0);


component redux6 is
	port(ina, inb, inc, ind, ine, inf : in std_logic;
  suma : out std_logic_vector(2 downto 0));
end component;

begin

chivato <= ref(16543 downto 16352); 
ultimos <= ref(95 downto 0);


usig00 : redux6 port map(resta0(8), resta1(8), resta2(8), resta3(8), resta4(8), resta5(8), signos00);
usig01 : redux6 port map(resta6(8), resta7(8), resta8(8), resta9(8), resta10(8), resta11(8), signos01);
usig02 : redux6 port map(resta12(8), resta13(8), resta14(8), resta15(8), resta16(8), resta17(8), signos02);
usig03 : redux6 port map(resta18(8), resta19(8), resta20(8), resta21(8), resta22(8), resta23(8), signos03);
usig04 : redux6 port map(resta24(8), resta25(8), resta26(8), resta27(8), resta28(8), resta29(8), signos04);
usig05 : redux6 port map(resta30(8), resta31(8), resta32(8), resta33(8), resta34(8), resta35(8), signos05);
usig06 : redux6 port map(resta36(8), resta37(8), resta38(8), resta39(8), resta40(8), resta41(8), signos06);
usig07 : redux6 port map(resta42(8), resta43(8), resta44(8), resta45(8), resta46(8), resta47(8), signos07);
usig08 : redux6 port map(resta48(8), resta49(8), resta50(8), resta51(8), resta52(8), resta53(8), signos08);
usig09 : redux6 port map(resta54(8), resta55(8), resta56(8), resta57(8), resta58(8), resta59(8), signos09);
usig10 : redux6 port map(resta60(8), resta61(8), resta62(8), resta63(8), resta64(8), resta65(8), signos10);
usig11 : redux6 port map(resta66(8), resta67(8), resta68(8), resta69(8), resta70(8), resta71(8), signos11);
usig12 : redux6 port map(resta72(8), resta73(8), resta74(8), resta75(8), resta76(8), resta77(8), signos12);
usig13 : redux6 port map(resta78(8), resta79(8), resta80(8), resta81(8), resta82(8), resta83(8), signos13);
usig14 : redux6 port map(resta84(8), resta85(8), resta86(8), resta87(8), resta88(8), resta89(8), signos14);
usig15 : redux6 port map(resta90(8), resta91(8), resta92(8), resta93(8), resta94(8), resta95(8), signos15);
usig16 : redux6 port map(resta96(8), resta97(8), resta98(8), resta99(8), resta100(8), resta101(8), signos16);
usig17 : redux6 port map(resta102(8), resta103(8), resta104(8), resta105(8), resta106(8), resta107(8), signos17);
usig18 : redux6 port map(resta108(8), resta109(8), resta110(8), resta111(8), resta112(8), resta113(8), signos18);
usig19 : redux6 port map(resta114(8), resta115(8), resta116(8), resta117(8), resta118(8), resta119(8), signos19);
usig20 : redux6 port map(resta120(8), resta121(8), resta122(8), resta123(8), resta124(8), resta125(8), signos20);
usig21 : redux6 port map(resta126(8), resta127(8), resta128(8), resta129(8), resta130(8), resta131(8), signos21);
usig22 : redux6 port map(resta132(8), resta133(8), resta134(8), resta135(8), resta136(8), resta137(8), signos22);
usig23 : redux6 port map(resta138(8), resta139(8), resta140(8), resta141(8), resta142(8), resta143(8), signos23);
usig24 : redux6 port map(resta144(8), resta145(8), resta146(8), resta147(8), resta148(8), resta149(8), signos24);
usig25 : redux6 port map(resta150(8), resta151(8), resta152(8), resta153(8), resta154(8), resta155(8), signos25);
usig26 : redux6 port map(resta156(8), resta157(8), resta158(8), resta159(8), resta160(8), resta161(8), signos26);
usig27 : redux6 port map(resta162(8), resta163(8), resta164(8), resta165(8), resta166(8), resta167(8), signos27);
usig28 : redux6 port map(resta168(8), resta169(8), resta170(8), resta171(8), resta172(8), resta173(8), signos28);
usig29 : redux6 port map(resta174(8), resta175(8), resta176(8), resta177(8), resta178(8), resta179(8), signos29);
usig30 : redux6 port map(resta180(8), resta181(8), resta182(8), resta183(8), resta184(8), resta185(8), signos30);
usig31 : redux6 port map(resta186(8), resta187(8), resta188(8), resta189(8), resta190(8), resta191(8), signos31);
usig32 : redux6 port map(resta192(8), resta193(8), resta194(8), resta195(8), resta196(8), resta197(8), signos32);
usig33 : redux6 port map(resta198(8), resta199(8), resta200(8), resta201(8), resta202(8), resta203(8), signos33);
usig34 : redux6 port map(resta204(8), resta205(8), resta206(8), resta207(8), resta208(8), resta209(8), signos34);
usig35 : redux6 port map(resta210(8), resta211(8), resta212(8), resta213(8), resta214(8), resta215(8), signos35);
usig36 : redux6 port map(resta216(8), resta217(8), resta218(8), resta219(8), resta220(8), resta221(8), signos36);
usig37 : redux6 port map(resta222(8), resta223(8), resta224(8), resta225(8), resta226(8), resta227(8), signos37);
usig38 : redux6 port map(resta228(8), resta229(8), resta230(8), resta231(8), resta232(8), resta233(8), signos38);
usig39 : redux6 port map(resta234(8), resta235(8), resta236(8), resta237(8), resta238(8), resta239(8), signos39);
usig40 : redux6 port map(resta240(8), resta241(8), resta242(8), resta243(8), resta244(8), resta245(8), signos40);
usig41 : redux6 port map(resta246(8), resta247(8), resta248(8), resta249(8), resta250(8), resta251(8), signos41);
usig42 : redux6 port map(resta252(8), resta253(8), resta254(8), resta255(8), '0', '0', signos42);




-- lo que queda...

SAD <= ("00" & three0) + ("00" & three1) + ("00" & three2) + ("00" & three3) + sigThree;



process(clk, act, ref, shift, dataIn, 
resta0, resta1, resta2, resta3, resta4, resta5, resta6, resta7, resta8, resta9, resta10, resta11, resta12, resta13, resta14, resta15, resta16, resta17, resta18, resta19, resta20, resta21, resta22, resta23, resta24, resta25, resta26, resta27, resta28, resta29, resta30, resta31, resta32, resta33, resta34, resta35, resta36, resta37, resta38, resta39, resta40, resta41, resta42, resta43, resta44, resta45, resta46, resta47, resta48, resta49, resta50, resta51, resta52, resta53, resta54, resta55, resta56, resta57, resta58, resta59, resta60, resta61, resta62, resta63, resta64, resta65, resta66, resta67, resta68, resta69, resta70, resta71, resta72, resta73, resta74, resta75, resta76, resta77, resta78, resta79, resta80, resta81, resta82, resta83, resta84, resta85, resta86, resta87, resta88, resta89, resta90, resta91, resta92, resta93, resta94, resta95, resta96, resta97, resta98, resta99, resta100, resta101, resta102, resta103, resta104, resta105, resta106, resta107, resta108, resta109, resta110, resta111, resta112, resta113, resta114, resta115, resta116, resta117, resta118, resta119, resta120, resta121, resta122, resta123, resta124, resta125, resta126, resta127, resta128, resta129, resta130, resta131, resta132, resta133, resta134, resta135, resta136, resta137, resta138, resta139, resta140, resta141, resta142, resta143, resta144, resta145, resta146, resta147, resta148, resta149, resta150, resta151, resta152, resta153, resta154, resta155, resta156, resta157, resta158, resta159, resta160, resta161, resta162, resta163, resta164, resta165, resta166, resta167, resta168, resta169, resta170, resta171, resta172, resta173, resta174, resta175, resta176, resta177, resta178, resta179, resta180, resta181, resta182, resta183, resta184, resta185, resta186, resta187, resta188, resta189, resta190, resta191, resta192, resta193, resta194, resta195, resta196, resta197, resta198, resta199, resta200, resta201, resta202, resta203, resta204, resta205, resta206, resta207, resta208, resta209, resta210, resta211, resta212, resta213, resta214, resta215, resta216, resta217, resta218, resta219, resta220, resta221, resta222, resta223, resta224, resta225, resta226, resta227, resta228, resta229, resta230, resta231, resta232, resta233, resta234, resta235, resta236, resta237, resta238, resta239, resta240, resta241, resta242, resta243, resta244, resta245, resta246, resta247, resta248, resta249, resta250, resta251, resta252, resta253, resta254, resta255, 
corr0, corr1, corr2, corr3, corr4, corr5, corr6, corr7, corr8, corr9, corr10, corr11, corr12, corr13, corr14, corr15, corr16, corr17, corr18, corr19, corr20, corr21, corr22, corr23, corr24, corr25, corr26, corr27, corr28, corr29, corr30, corr31, corr32, corr33, corr34, corr35, corr36, corr37, corr38, corr39, corr40, corr41, corr42, corr43, corr44, corr45, corr46, corr47, corr48, corr49, corr50, corr51, corr52, corr53, corr54, corr55, corr56, corr57, corr58, corr59, corr60, corr61, corr62, corr63, corr64, corr65, corr66, corr67, corr68, corr69, corr70, corr71, corr72, corr73, corr74, corr75, corr76, corr77, corr78, corr79, corr80, corr81, corr82, corr83, corr84, corr85, corr86, corr87, corr88, corr89, corr90, corr91, corr92, corr93, corr94, corr95, corr96, corr97, corr98, corr99, corr100, corr101, corr102, corr103, corr104, corr105, corr106, corr107, corr108, corr109, corr110, corr111, corr112, corr113, corr114, corr115, corr116, corr117, corr118, corr119, corr120, corr121, corr122, corr123, corr124, corr125, corr126, corr127, corr128, corr129, corr130, corr131, corr132, corr133, corr134, corr135, corr136, corr137, corr138, corr139, corr140, corr141, corr142, corr143, corr144, corr145, corr146, corr147, corr148, corr149, corr150, corr151, corr152, corr153, corr154, corr155, corr156, corr157, corr158, corr159, corr160, corr161, corr162, corr163, corr164, corr165, corr166, corr167, corr168, corr169, corr170, corr171, corr172, corr173, corr174, corr175, corr176, corr177, corr178, corr179, corr180, corr181, corr182, corr183, corr184, corr185, corr186, corr187, corr188, corr189, corr190, corr191, corr192, corr193, corr194, corr195, corr196, corr197, corr198, corr199, corr200, corr201, corr202, corr203, corr204, corr205, corr206, corr207, corr208, corr209, corr210, corr211, corr212, corr213, corr214, corr215, corr216, corr217, corr218, corr219, corr220, corr221, corr222, corr223, corr224, corr225, corr226, corr227, corr228, corr229, corr230, corr231, corr232, corr233, corr234, corr235, corr236, corr237, corr238, corr239, corr240, corr241, corr242, corr243, corr244, corr245, corr246, corr247, corr248, corr249, corr250, corr251, corr252, corr253, corr254, corr255, 
one0, one1, one2, one3, one4, one5, one6, one7, one8, one9, one10, one11, one12, one13, one14, one15, one16, one17, one18, one19, one20, one21, one22, one23, one24, one25, one26, one27, one28, one29, one30, one31, one32, one33, one34, one35, one36, one37, one38, one39, one40, one41, one42, one43, one44, one45, one46, one47, one48, one49, one50, one51, one52, one53, one54, one55, one56, one57, one58, one59, one60, one61, one62, one63, 
two0, two1, two2, two3, two4, two5, two6, two7, two8, two9, two10, two11, two12, two13, two14, two15, 
three0, three1, three2, three3, 
signos00, signos01, signos02, signos03, signos04, signos05, signos06, signos07, signos08, signos09, signos10, signos11, signos12, signos13, signos14, signos15, signos16, signos17, signos18, signos19, signos20, signos21, signos22, signos23, signos24, signos25, signos26, signos27, signos28, signos29, signos30, signos31, signos32, signos33, signos34, signos35, signos36, signos37, signos38, signos39, signos40, signos41, signos42,  
regSignos00, regSignos01, regSignos02, regSignos03, regSignos04, regSignos05, regSignos06, regSignos07, regSignos08, regSignos09, regSignos10, regSignos11, regSignos12, regSignos13, regSignos14, regSignos15, regSignos16, regSignos17, regSignos18, regSignos19, regSignos20, regSignos21, regSignos22, regSignos23, regSignos24, regSignos25, regSignos26, regSignos27, regSignos28, regSignos29, regSignos30, regSignos31, regSignos32, regSignos33, regSignos34, regSignos35, regSignos36, regSignos37, regSignos38, regSignos39, regSignos40, regSignos41, regSignos42, 
sigOne0, sigOne1, sigOne2, sigOne3, sigOne4, sigOne5, sigOne6, sigOne7, sigOne8, sigOne9, sigOne10, 
sigTwo0, sigTwo1, sigTwo2, 
sigThree)
begin
    -- no es necesario reset
    if rising_edge(clk) then
       
resta0 <=  ("0" & act(2047 downto 2040)) + ("1" & not ref(17663 downto 17656)) + '1'; 
resta1 <=  ("0" & act(2039 downto 2032)) + ("1" & not ref(17655 downto 17648)) + '1'; 
resta2 <=  ("0" & act(2031 downto 2024)) + ("1" & not ref(17647 downto 17640)) + '1'; 
resta3 <=  ("0" & act(2023 downto 2016)) + ("1" & not ref(17639 downto 17632)) + '1'; 
resta4 <=  ("0" & act(2015 downto 2008)) + ("1" & not ref(17631 downto 17624)) + '1'; 
resta5 <=  ("0" & act(2007 downto 2000)) + ("1" & not ref(17623 downto 17616)) + '1'; 
resta6 <=  ("0" & act(1999 downto 1992)) + ("1" & not ref(17615 downto 17608)) + '1'; 
resta7 <=  ("0" & act(1991 downto 1984)) + ("1" & not ref(17607 downto 17600)) + '1'; 
resta8 <=  ("0" & act(1983 downto 1976)) + ("1" & not ref(17599 downto 17592)) + '1'; 
resta9 <=  ("0" & act(1975 downto 1968)) + ("1" & not ref(17591 downto 17584)) + '1'; 
resta10 <=  ("0" & act(1967 downto 1960)) + ("1" & not ref(17583 downto 17576)) + '1'; 
resta11 <=  ("0" & act(1959 downto 1952)) + ("1" & not ref(17575 downto 17568)) + '1'; 
resta12 <=  ("0" & act(1951 downto 1944)) + ("1" & not ref(17567 downto 17560)) + '1'; 
resta13 <=  ("0" & act(1943 downto 1936)) + ("1" & not ref(17559 downto 17552)) + '1'; 
resta14 <=  ("0" & act(1935 downto 1928)) + ("1" & not ref(17551 downto 17544)) + '1'; 
resta15 <=  ("0" & act(1927 downto 1920)) + ("1" & not ref(17543 downto 17536)) + '1'; 
resta16 <=  ("0" & act(1919 downto 1912)) + ("1" & not ref(17279 downto 17272)) + '1'; 
resta17 <=  ("0" & act(1911 downto 1904)) + ("1" & not ref(17271 downto 17264)) + '1'; 
resta18 <=  ("0" & act(1903 downto 1896)) + ("1" & not ref(17263 downto 17256)) + '1'; 
resta19 <=  ("0" & act(1895 downto 1888)) + ("1" & not ref(17255 downto 17248)) + '1'; 
resta20 <=  ("0" & act(1887 downto 1880)) + ("1" & not ref(17247 downto 17240)) + '1'; 
resta21 <=  ("0" & act(1879 downto 1872)) + ("1" & not ref(17239 downto 17232)) + '1'; 
resta22 <=  ("0" & act(1871 downto 1864)) + ("1" & not ref(17231 downto 17224)) + '1'; 
resta23 <=  ("0" & act(1863 downto 1856)) + ("1" & not ref(17223 downto 17216)) + '1'; 
resta24 <=  ("0" & act(1855 downto 1848)) + ("1" & not ref(17215 downto 17208)) + '1'; 
resta25 <=  ("0" & act(1847 downto 1840)) + ("1" & not ref(17207 downto 17200)) + '1'; 
resta26 <=  ("0" & act(1839 downto 1832)) + ("1" & not ref(17199 downto 17192)) + '1'; 
resta27 <=  ("0" & act(1831 downto 1824)) + ("1" & not ref(17191 downto 17184)) + '1'; 
resta28 <=  ("0" & act(1823 downto 1816)) + ("1" & not ref(17183 downto 17176)) + '1'; 
resta29 <=  ("0" & act(1815 downto 1808)) + ("1" & not ref(17175 downto 17168)) + '1'; 
resta30 <=  ("0" & act(1807 downto 1800)) + ("1" & not ref(17167 downto 17160)) + '1'; 
resta31 <=  ("0" & act(1799 downto 1792)) + ("1" & not ref(17159 downto 17152)) + '1'; 
resta32 <=  ("0" & act(1791 downto 1784)) + ("1" & not ref(16895 downto 16888)) + '1'; 
resta33 <=  ("0" & act(1783 downto 1776)) + ("1" & not ref(16887 downto 16880)) + '1'; 
resta34 <=  ("0" & act(1775 downto 1768)) + ("1" & not ref(16879 downto 16872)) + '1'; 
resta35 <=  ("0" & act(1767 downto 1760)) + ("1" & not ref(16871 downto 16864)) + '1'; 
resta36 <=  ("0" & act(1759 downto 1752)) + ("1" & not ref(16863 downto 16856)) + '1'; 
resta37 <=  ("0" & act(1751 downto 1744)) + ("1" & not ref(16855 downto 16848)) + '1'; 
resta38 <=  ("0" & act(1743 downto 1736)) + ("1" & not ref(16847 downto 16840)) + '1'; 
resta39 <=  ("0" & act(1735 downto 1728)) + ("1" & not ref(16839 downto 16832)) + '1'; 
resta40 <=  ("0" & act(1727 downto 1720)) + ("1" & not ref(16831 downto 16824)) + '1'; 
resta41 <=  ("0" & act(1719 downto 1712)) + ("1" & not ref(16823 downto 16816)) + '1'; 
resta42 <=  ("0" & act(1711 downto 1704)) + ("1" & not ref(16815 downto 16808)) + '1'; 
resta43 <=  ("0" & act(1703 downto 1696)) + ("1" & not ref(16807 downto 16800)) + '1'; 
resta44 <=  ("0" & act(1695 downto 1688)) + ("1" & not ref(16799 downto 16792)) + '1'; 
resta45 <=  ("0" & act(1687 downto 1680)) + ("1" & not ref(16791 downto 16784)) + '1'; 
resta46 <=  ("0" & act(1679 downto 1672)) + ("1" & not ref(16783 downto 16776)) + '1'; 
resta47 <=  ("0" & act(1671 downto 1664)) + ("1" & not ref(16775 downto 16768)) + '1'; 
resta48 <=  ("0" & act(1663 downto 1656)) + ("1" & not ref(16511 downto 16504)) + '1'; 
resta49 <=  ("0" & act(1655 downto 1648)) + ("1" & not ref(16503 downto 16496)) + '1'; 
resta50 <=  ("0" & act(1647 downto 1640)) + ("1" & not ref(16495 downto 16488)) + '1'; 
resta51 <=  ("0" & act(1639 downto 1632)) + ("1" & not ref(16487 downto 16480)) + '1'; 
resta52 <=  ("0" & act(1631 downto 1624)) + ("1" & not ref(16479 downto 16472)) + '1'; 
resta53 <=  ("0" & act(1623 downto 1616)) + ("1" & not ref(16471 downto 16464)) + '1'; 
resta54 <=  ("0" & act(1615 downto 1608)) + ("1" & not ref(16463 downto 16456)) + '1'; 
resta55 <=  ("0" & act(1607 downto 1600)) + ("1" & not ref(16455 downto 16448)) + '1'; 
resta56 <=  ("0" & act(1599 downto 1592)) + ("1" & not ref(16447 downto 16440)) + '1'; 
resta57 <=  ("0" & act(1591 downto 1584)) + ("1" & not ref(16439 downto 16432)) + '1'; 
resta58 <=  ("0" & act(1583 downto 1576)) + ("1" & not ref(16431 downto 16424)) + '1'; 
resta59 <=  ("0" & act(1575 downto 1568)) + ("1" & not ref(16423 downto 16416)) + '1'; 
resta60 <=  ("0" & act(1567 downto 1560)) + ("1" & not ref(16415 downto 16408)) + '1'; 
resta61 <=  ("0" & act(1559 downto 1552)) + ("1" & not ref(16407 downto 16400)) + '1'; 
resta62 <=  ("0" & act(1551 downto 1544)) + ("1" & not ref(16399 downto 16392)) + '1'; 
resta63 <=  ("0" & act(1543 downto 1536)) + ("1" & not ref(16391 downto 16384)) + '1'; 
resta64 <=  ("0" & act(1535 downto 1528)) + ("1" & not ref(16127 downto 16120)) + '1'; 
resta65 <=  ("0" & act(1527 downto 1520)) + ("1" & not ref(16119 downto 16112)) + '1'; 
resta66 <=  ("0" & act(1519 downto 1512)) + ("1" & not ref(16111 downto 16104)) + '1'; 
resta67 <=  ("0" & act(1511 downto 1504)) + ("1" & not ref(16103 downto 16096)) + '1'; 
resta68 <=  ("0" & act(1503 downto 1496)) + ("1" & not ref(16095 downto 16088)) + '1'; 
resta69 <=  ("0" & act(1495 downto 1488)) + ("1" & not ref(16087 downto 16080)) + '1'; 
resta70 <=  ("0" & act(1487 downto 1480)) + ("1" & not ref(16079 downto 16072)) + '1'; 
resta71 <=  ("0" & act(1479 downto 1472)) + ("1" & not ref(16071 downto 16064)) + '1'; 
resta72 <=  ("0" & act(1471 downto 1464)) + ("1" & not ref(16063 downto 16056)) + '1'; 
resta73 <=  ("0" & act(1463 downto 1456)) + ("1" & not ref(16055 downto 16048)) + '1'; 
resta74 <=  ("0" & act(1455 downto 1448)) + ("1" & not ref(16047 downto 16040)) + '1'; 
resta75 <=  ("0" & act(1447 downto 1440)) + ("1" & not ref(16039 downto 16032)) + '1'; 
resta76 <=  ("0" & act(1439 downto 1432)) + ("1" & not ref(16031 downto 16024)) + '1'; 
resta77 <=  ("0" & act(1431 downto 1424)) + ("1" & not ref(16023 downto 16016)) + '1'; 
resta78 <=  ("0" & act(1423 downto 1416)) + ("1" & not ref(16015 downto 16008)) + '1'; 
resta79 <=  ("0" & act(1415 downto 1408)) + ("1" & not ref(16007 downto 16000)) + '1'; 
resta80 <=  ("0" & act(1407 downto 1400)) + ("1" & not ref(15743 downto 15736)) + '1'; 
resta81 <=  ("0" & act(1399 downto 1392)) + ("1" & not ref(15735 downto 15728)) + '1'; 
resta82 <=  ("0" & act(1391 downto 1384)) + ("1" & not ref(15727 downto 15720)) + '1'; 
resta83 <=  ("0" & act(1383 downto 1376)) + ("1" & not ref(15719 downto 15712)) + '1'; 
resta84 <=  ("0" & act(1375 downto 1368)) + ("1" & not ref(15711 downto 15704)) + '1'; 
resta85 <=  ("0" & act(1367 downto 1360)) + ("1" & not ref(15703 downto 15696)) + '1'; 
resta86 <=  ("0" & act(1359 downto 1352)) + ("1" & not ref(15695 downto 15688)) + '1'; 
resta87 <=  ("0" & act(1351 downto 1344)) + ("1" & not ref(15687 downto 15680)) + '1'; 
resta88 <=  ("0" & act(1343 downto 1336)) + ("1" & not ref(15679 downto 15672)) + '1'; 
resta89 <=  ("0" & act(1335 downto 1328)) + ("1" & not ref(15671 downto 15664)) + '1'; 
resta90 <=  ("0" & act(1327 downto 1320)) + ("1" & not ref(15663 downto 15656)) + '1'; 
resta91 <=  ("0" & act(1319 downto 1312)) + ("1" & not ref(15655 downto 15648)) + '1'; 
resta92 <=  ("0" & act(1311 downto 1304)) + ("1" & not ref(15647 downto 15640)) + '1'; 
resta93 <=  ("0" & act(1303 downto 1296)) + ("1" & not ref(15639 downto 15632)) + '1'; 
resta94 <=  ("0" & act(1295 downto 1288)) + ("1" & not ref(15631 downto 15624)) + '1'; 
resta95 <=  ("0" & act(1287 downto 1280)) + ("1" & not ref(15623 downto 15616)) + '1'; 
resta96 <=  ("0" & act(1279 downto 1272)) + ("1" & not ref(15359 downto 15352)) + '1'; 
resta97 <=  ("0" & act(1271 downto 1264)) + ("1" & not ref(15351 downto 15344)) + '1'; 
resta98 <=  ("0" & act(1263 downto 1256)) + ("1" & not ref(15343 downto 15336)) + '1'; 
resta99 <=  ("0" & act(1255 downto 1248)) + ("1" & not ref(15335 downto 15328)) + '1'; 
resta100 <=  ("0" & act(1247 downto 1240)) + ("1" & not ref(15327 downto 15320)) + '1'; 
resta101 <=  ("0" & act(1239 downto 1232)) + ("1" & not ref(15319 downto 15312)) + '1'; 
resta102 <=  ("0" & act(1231 downto 1224)) + ("1" & not ref(15311 downto 15304)) + '1'; 
resta103 <=  ("0" & act(1223 downto 1216)) + ("1" & not ref(15303 downto 15296)) + '1'; 
resta104 <=  ("0" & act(1215 downto 1208)) + ("1" & not ref(15295 downto 15288)) + '1'; 
resta105 <=  ("0" & act(1207 downto 1200)) + ("1" & not ref(15287 downto 15280)) + '1'; 
resta106 <=  ("0" & act(1199 downto 1192)) + ("1" & not ref(15279 downto 15272)) + '1'; 
resta107 <=  ("0" & act(1191 downto 1184)) + ("1" & not ref(15271 downto 15264)) + '1'; 
resta108 <=  ("0" & act(1183 downto 1176)) + ("1" & not ref(15263 downto 15256)) + '1'; 
resta109 <=  ("0" & act(1175 downto 1168)) + ("1" & not ref(15255 downto 15248)) + '1'; 
resta110 <=  ("0" & act(1167 downto 1160)) + ("1" & not ref(15247 downto 15240)) + '1'; 
resta111 <=  ("0" & act(1159 downto 1152)) + ("1" & not ref(15239 downto 15232)) + '1'; 
resta112 <=  ("0" & act(1151 downto 1144)) + ("1" & not ref(14975 downto 14968)) + '1'; 
resta113 <=  ("0" & act(1143 downto 1136)) + ("1" & not ref(14967 downto 14960)) + '1'; 
resta114 <=  ("0" & act(1135 downto 1128)) + ("1" & not ref(14959 downto 14952)) + '1'; 
resta115 <=  ("0" & act(1127 downto 1120)) + ("1" & not ref(14951 downto 14944)) + '1'; 
resta116 <=  ("0" & act(1119 downto 1112)) + ("1" & not ref(14943 downto 14936)) + '1'; 
resta117 <=  ("0" & act(1111 downto 1104)) + ("1" & not ref(14935 downto 14928)) + '1'; 
resta118 <=  ("0" & act(1103 downto 1096)) + ("1" & not ref(14927 downto 14920)) + '1'; 
resta119 <=  ("0" & act(1095 downto 1088)) + ("1" & not ref(14919 downto 14912)) + '1'; 
resta120 <=  ("0" & act(1087 downto 1080)) + ("1" & not ref(14911 downto 14904)) + '1'; 
resta121 <=  ("0" & act(1079 downto 1072)) + ("1" & not ref(14903 downto 14896)) + '1'; 
resta122 <=  ("0" & act(1071 downto 1064)) + ("1" & not ref(14895 downto 14888)) + '1'; 
resta123 <=  ("0" & act(1063 downto 1056)) + ("1" & not ref(14887 downto 14880)) + '1'; 
resta124 <=  ("0" & act(1055 downto 1048)) + ("1" & not ref(14879 downto 14872)) + '1'; 
resta125 <=  ("0" & act(1047 downto 1040)) + ("1" & not ref(14871 downto 14864)) + '1'; 
resta126 <=  ("0" & act(1039 downto 1032)) + ("1" & not ref(14863 downto 14856)) + '1'; 
resta127 <=  ("0" & act(1031 downto 1024)) + ("1" & not ref(14855 downto 14848)) + '1'; 
resta128 <=  ("0" & act(1023 downto 1016)) + ("1" & not ref(14591 downto 14584)) + '1'; 
resta129 <=  ("0" & act(1015 downto 1008)) + ("1" & not ref(14583 downto 14576)) + '1'; 
resta130 <=  ("0" & act(1007 downto 1000)) + ("1" & not ref(14575 downto 14568)) + '1'; 
resta131 <=  ("0" & act(999 downto 992)) + ("1" & not ref(14567 downto 14560)) + '1'; 
resta132 <=  ("0" & act(991 downto 984)) + ("1" & not ref(14559 downto 14552)) + '1'; 
resta133 <=  ("0" & act(983 downto 976)) + ("1" & not ref(14551 downto 14544)) + '1'; 
resta134 <=  ("0" & act(975 downto 968)) + ("1" & not ref(14543 downto 14536)) + '1'; 
resta135 <=  ("0" & act(967 downto 960)) + ("1" & not ref(14535 downto 14528)) + '1'; 
resta136 <=  ("0" & act(959 downto 952)) + ("1" & not ref(14527 downto 14520)) + '1'; 
resta137 <=  ("0" & act(951 downto 944)) + ("1" & not ref(14519 downto 14512)) + '1'; 
resta138 <=  ("0" & act(943 downto 936)) + ("1" & not ref(14511 downto 14504)) + '1'; 
resta139 <=  ("0" & act(935 downto 928)) + ("1" & not ref(14503 downto 14496)) + '1'; 
resta140 <=  ("0" & act(927 downto 920)) + ("1" & not ref(14495 downto 14488)) + '1'; 
resta141 <=  ("0" & act(919 downto 912)) + ("1" & not ref(14487 downto 14480)) + '1'; 
resta142 <=  ("0" & act(911 downto 904)) + ("1" & not ref(14479 downto 14472)) + '1'; 
resta143 <=  ("0" & act(903 downto 896)) + ("1" & not ref(14471 downto 14464)) + '1'; 
resta144 <=  ("0" & act(895 downto 888)) + ("1" & not ref(14207 downto 14200)) + '1'; 
resta145 <=  ("0" & act(887 downto 880)) + ("1" & not ref(14199 downto 14192)) + '1'; 
resta146 <=  ("0" & act(879 downto 872)) + ("1" & not ref(14191 downto 14184)) + '1'; 
resta147 <=  ("0" & act(871 downto 864)) + ("1" & not ref(14183 downto 14176)) + '1'; 
resta148 <=  ("0" & act(863 downto 856)) + ("1" & not ref(14175 downto 14168)) + '1'; 
resta149 <=  ("0" & act(855 downto 848)) + ("1" & not ref(14167 downto 14160)) + '1'; 
resta150 <=  ("0" & act(847 downto 840)) + ("1" & not ref(14159 downto 14152)) + '1'; 
resta151 <=  ("0" & act(839 downto 832)) + ("1" & not ref(14151 downto 14144)) + '1'; 
resta152 <=  ("0" & act(831 downto 824)) + ("1" & not ref(14143 downto 14136)) + '1'; 
resta153 <=  ("0" & act(823 downto 816)) + ("1" & not ref(14135 downto 14128)) + '1'; 
resta154 <=  ("0" & act(815 downto 808)) + ("1" & not ref(14127 downto 14120)) + '1'; 
resta155 <=  ("0" & act(807 downto 800)) + ("1" & not ref(14119 downto 14112)) + '1'; 
resta156 <=  ("0" & act(799 downto 792)) + ("1" & not ref(14111 downto 14104)) + '1'; 
resta157 <=  ("0" & act(791 downto 784)) + ("1" & not ref(14103 downto 14096)) + '1'; 
resta158 <=  ("0" & act(783 downto 776)) + ("1" & not ref(14095 downto 14088)) + '1'; 
resta159 <=  ("0" & act(775 downto 768)) + ("1" & not ref(14087 downto 14080)) + '1'; 
resta160 <=  ("0" & act(767 downto 760)) + ("1" & not ref(13823 downto 13816)) + '1'; 
resta161 <=  ("0" & act(759 downto 752)) + ("1" & not ref(13815 downto 13808)) + '1'; 
resta162 <=  ("0" & act(751 downto 744)) + ("1" & not ref(13807 downto 13800)) + '1'; 
resta163 <=  ("0" & act(743 downto 736)) + ("1" & not ref(13799 downto 13792)) + '1'; 
resta164 <=  ("0" & act(735 downto 728)) + ("1" & not ref(13791 downto 13784)) + '1'; 
resta165 <=  ("0" & act(727 downto 720)) + ("1" & not ref(13783 downto 13776)) + '1'; 
resta166 <=  ("0" & act(719 downto 712)) + ("1" & not ref(13775 downto 13768)) + '1'; 
resta167 <=  ("0" & act(711 downto 704)) + ("1" & not ref(13767 downto 13760)) + '1'; 
resta168 <=  ("0" & act(703 downto 696)) + ("1" & not ref(13759 downto 13752)) + '1'; 
resta169 <=  ("0" & act(695 downto 688)) + ("1" & not ref(13751 downto 13744)) + '1'; 
resta170 <=  ("0" & act(687 downto 680)) + ("1" & not ref(13743 downto 13736)) + '1'; 
resta171 <=  ("0" & act(679 downto 672)) + ("1" & not ref(13735 downto 13728)) + '1'; 
resta172 <=  ("0" & act(671 downto 664)) + ("1" & not ref(13727 downto 13720)) + '1'; 
resta173 <=  ("0" & act(663 downto 656)) + ("1" & not ref(13719 downto 13712)) + '1'; 
resta174 <=  ("0" & act(655 downto 648)) + ("1" & not ref(13711 downto 13704)) + '1'; 
resta175 <=  ("0" & act(647 downto 640)) + ("1" & not ref(13703 downto 13696)) + '1'; 
resta176 <=  ("0" & act(639 downto 632)) + ("1" & not ref(13439 downto 13432)) + '1'; 
resta177 <=  ("0" & act(631 downto 624)) + ("1" & not ref(13431 downto 13424)) + '1'; 
resta178 <=  ("0" & act(623 downto 616)) + ("1" & not ref(13423 downto 13416)) + '1'; 
resta179 <=  ("0" & act(615 downto 608)) + ("1" & not ref(13415 downto 13408)) + '1'; 
resta180 <=  ("0" & act(607 downto 600)) + ("1" & not ref(13407 downto 13400)) + '1'; 
resta181 <=  ("0" & act(599 downto 592)) + ("1" & not ref(13399 downto 13392)) + '1'; 
resta182 <=  ("0" & act(591 downto 584)) + ("1" & not ref(13391 downto 13384)) + '1'; 
resta183 <=  ("0" & act(583 downto 576)) + ("1" & not ref(13383 downto 13376)) + '1'; 
resta184 <=  ("0" & act(575 downto 568)) + ("1" & not ref(13375 downto 13368)) + '1'; 
resta185 <=  ("0" & act(567 downto 560)) + ("1" & not ref(13367 downto 13360)) + '1'; 
resta186 <=  ("0" & act(559 downto 552)) + ("1" & not ref(13359 downto 13352)) + '1'; 
resta187 <=  ("0" & act(551 downto 544)) + ("1" & not ref(13351 downto 13344)) + '1'; 
resta188 <=  ("0" & act(543 downto 536)) + ("1" & not ref(13343 downto 13336)) + '1'; 
resta189 <=  ("0" & act(535 downto 528)) + ("1" & not ref(13335 downto 13328)) + '1'; 
resta190 <=  ("0" & act(527 downto 520)) + ("1" & not ref(13327 downto 13320)) + '1'; 
resta191 <=  ("0" & act(519 downto 512)) + ("1" & not ref(13319 downto 13312)) + '1'; 
resta192 <=  ("0" & act(511 downto 504)) + ("1" & not ref(13055 downto 13048)) + '1'; 
resta193 <=  ("0" & act(503 downto 496)) + ("1" & not ref(13047 downto 13040)) + '1'; 
resta194 <=  ("0" & act(495 downto 488)) + ("1" & not ref(13039 downto 13032)) + '1'; 
resta195 <=  ("0" & act(487 downto 480)) + ("1" & not ref(13031 downto 13024)) + '1'; 
resta196 <=  ("0" & act(479 downto 472)) + ("1" & not ref(13023 downto 13016)) + '1'; 
resta197 <=  ("0" & act(471 downto 464)) + ("1" & not ref(13015 downto 13008)) + '1'; 
resta198 <=  ("0" & act(463 downto 456)) + ("1" & not ref(13007 downto 13000)) + '1'; 
resta199 <=  ("0" & act(455 downto 448)) + ("1" & not ref(12999 downto 12992)) + '1'; 
resta200 <=  ("0" & act(447 downto 440)) + ("1" & not ref(12991 downto 12984)) + '1'; 
resta201 <=  ("0" & act(439 downto 432)) + ("1" & not ref(12983 downto 12976)) + '1'; 
resta202 <=  ("0" & act(431 downto 424)) + ("1" & not ref(12975 downto 12968)) + '1'; 
resta203 <=  ("0" & act(423 downto 416)) + ("1" & not ref(12967 downto 12960)) + '1'; 
resta204 <=  ("0" & act(415 downto 408)) + ("1" & not ref(12959 downto 12952)) + '1'; 
resta205 <=  ("0" & act(407 downto 400)) + ("1" & not ref(12951 downto 12944)) + '1'; 
resta206 <=  ("0" & act(399 downto 392)) + ("1" & not ref(12943 downto 12936)) + '1'; 
resta207 <=  ("0" & act(391 downto 384)) + ("1" & not ref(12935 downto 12928)) + '1'; 
resta208 <=  ("0" & act(383 downto 376)) + ("1" & not ref(12671 downto 12664)) + '1'; 
resta209 <=  ("0" & act(375 downto 368)) + ("1" & not ref(12663 downto 12656)) + '1'; 
resta210 <=  ("0" & act(367 downto 360)) + ("1" & not ref(12655 downto 12648)) + '1'; 
resta211 <=  ("0" & act(359 downto 352)) + ("1" & not ref(12647 downto 12640)) + '1'; 
resta212 <=  ("0" & act(351 downto 344)) + ("1" & not ref(12639 downto 12632)) + '1'; 
resta213 <=  ("0" & act(343 downto 336)) + ("1" & not ref(12631 downto 12624)) + '1'; 
resta214 <=  ("0" & act(335 downto 328)) + ("1" & not ref(12623 downto 12616)) + '1'; 
resta215 <=  ("0" & act(327 downto 320)) + ("1" & not ref(12615 downto 12608)) + '1'; 
resta216 <=  ("0" & act(319 downto 312)) + ("1" & not ref(12607 downto 12600)) + '1'; 
resta217 <=  ("0" & act(311 downto 304)) + ("1" & not ref(12599 downto 12592)) + '1'; 
resta218 <=  ("0" & act(303 downto 296)) + ("1" & not ref(12591 downto 12584)) + '1'; 
resta219 <=  ("0" & act(295 downto 288)) + ("1" & not ref(12583 downto 12576)) + '1'; 
resta220 <=  ("0" & act(287 downto 280)) + ("1" & not ref(12575 downto 12568)) + '1'; 
resta221 <=  ("0" & act(279 downto 272)) + ("1" & not ref(12567 downto 12560)) + '1'; 
resta222 <=  ("0" & act(271 downto 264)) + ("1" & not ref(12559 downto 12552)) + '1'; 
resta223 <=  ("0" & act(263 downto 256)) + ("1" & not ref(12551 downto 12544)) + '1'; 
resta224 <=  ("0" & act(255 downto 248)) + ("1" & not ref(12287 downto 12280)) + '1'; 
resta225 <=  ("0" & act(247 downto 240)) + ("1" & not ref(12279 downto 12272)) + '1'; 
resta226 <=  ("0" & act(239 downto 232)) + ("1" & not ref(12271 downto 12264)) + '1'; 
resta227 <=  ("0" & act(231 downto 224)) + ("1" & not ref(12263 downto 12256)) + '1'; 
resta228 <=  ("0" & act(223 downto 216)) + ("1" & not ref(12255 downto 12248)) + '1'; 
resta229 <=  ("0" & act(215 downto 208)) + ("1" & not ref(12247 downto 12240)) + '1'; 
resta230 <=  ("0" & act(207 downto 200)) + ("1" & not ref(12239 downto 12232)) + '1'; 
resta231 <=  ("0" & act(199 downto 192)) + ("1" & not ref(12231 downto 12224)) + '1'; 
resta232 <=  ("0" & act(191 downto 184)) + ("1" & not ref(12223 downto 12216)) + '1'; 
resta233 <=  ("0" & act(183 downto 176)) + ("1" & not ref(12215 downto 12208)) + '1'; 
resta234 <=  ("0" & act(175 downto 168)) + ("1" & not ref(12207 downto 12200)) + '1'; 
resta235 <=  ("0" & act(167 downto 160)) + ("1" & not ref(12199 downto 12192)) + '1'; 
resta236 <=  ("0" & act(159 downto 152)) + ("1" & not ref(12191 downto 12184)) + '1'; 
resta237 <=  ("0" & act(151 downto 144)) + ("1" & not ref(12183 downto 12176)) + '1'; 
resta238 <=  ("0" & act(143 downto 136)) + ("1" & not ref(12175 downto 12168)) + '1'; 
resta239 <=  ("0" & act(135 downto 128)) + ("1" & not ref(12167 downto 12160)) + '1'; 
resta240 <=  ("0" & act(127 downto 120)) + ("1" & not ref(11903 downto 11896)) + '1'; 
resta241 <=  ("0" & act(119 downto 112)) + ("1" & not ref(11895 downto 11888)) + '1'; 
resta242 <=  ("0" & act(111 downto 104)) + ("1" & not ref(11887 downto 11880)) + '1'; 
resta243 <=  ("0" & act(103 downto 96)) + ("1" & not ref(11879 downto 11872)) + '1'; 
resta244 <=  ("0" & act(95 downto 88)) + ("1" & not ref(11871 downto 11864)) + '1'; 
resta245 <=  ("0" & act(87 downto 80)) + ("1" & not ref(11863 downto 11856)) + '1'; 
resta246 <=  ("0" & act(79 downto 72)) + ("1" & not ref(11855 downto 11848)) + '1'; 
resta247 <=  ("0" & act(71 downto 64)) + ("1" & not ref(11847 downto 11840)) + '1'; 
resta248 <=  ("0" & act(63 downto 56)) + ("1" & not ref(11839 downto 11832)) + '1'; 
resta249 <=  ("0" & act(55 downto 48)) + ("1" & not ref(11831 downto 11824)) + '1'; 
resta250 <=  ("0" & act(47 downto 40)) + ("1" & not ref(11823 downto 11816)) + '1'; 
resta251 <=  ("0" & act(39 downto 32)) + ("1" & not ref(11815 downto 11808)) + '1'; 
resta252 <=  ("0" & act(31 downto 24)) + ("1" & not ref(11807 downto 11800)) + '1'; 
resta253 <=  ("0" & act(23 downto 16)) + ("1" & not ref(11799 downto 11792)) + '1'; 
resta254 <=  ("0" & act(15 downto 8)) + ("1" & not ref(11791 downto 11784)) + '1'; 
resta255 <=  ("0" & act(7 downto 0)) + ("1" & not ref(11783 downto 11776)) + '1'; 

        
if resta0(8) = '0' then corr0 <= resta0(7 downto 0); else corr0 <=  not resta0(7 downto 0); end if;
if resta1(8) = '0' then corr1 <= resta1(7 downto 0); else corr1 <=  not resta1(7 downto 0); end if;
if resta2(8) = '0' then corr2 <= resta2(7 downto 0); else corr2 <=  not resta2(7 downto 0); end if;
if resta3(8) = '0' then corr3 <= resta3(7 downto 0); else corr3 <=  not resta3(7 downto 0); end if;
if resta4(8) = '0' then corr4 <= resta4(7 downto 0); else corr4 <=  not resta4(7 downto 0); end if;
if resta5(8) = '0' then corr5 <= resta5(7 downto 0); else corr5 <=  not resta5(7 downto 0); end if;
if resta6(8) = '0' then corr6 <= resta6(7 downto 0); else corr6 <=  not resta6(7 downto 0); end if;
if resta7(8) = '0' then corr7 <= resta7(7 downto 0); else corr7 <=  not resta7(7 downto 0); end if;
if resta8(8) = '0' then corr8 <= resta8(7 downto 0); else corr8 <=  not resta8(7 downto 0); end if;
if resta9(8) = '0' then corr9 <= resta9(7 downto 0); else corr9 <=  not resta9(7 downto 0); end if;
if resta10(8) = '0' then corr10 <= resta10(7 downto 0); else corr10 <=  not resta10(7 downto 0); end if;
if resta11(8) = '0' then corr11 <= resta11(7 downto 0); else corr11 <=  not resta11(7 downto 0); end if;
if resta12(8) = '0' then corr12 <= resta12(7 downto 0); else corr12 <=  not resta12(7 downto 0); end if;
if resta13(8) = '0' then corr13 <= resta13(7 downto 0); else corr13 <=  not resta13(7 downto 0); end if;
if resta14(8) = '0' then corr14 <= resta14(7 downto 0); else corr14 <=  not resta14(7 downto 0); end if;
if resta15(8) = '0' then corr15 <= resta15(7 downto 0); else corr15 <=  not resta15(7 downto 0); end if;
if resta16(8) = '0' then corr16 <= resta16(7 downto 0); else corr16 <=  not resta16(7 downto 0); end if;
if resta17(8) = '0' then corr17 <= resta17(7 downto 0); else corr17 <=  not resta17(7 downto 0); end if;
if resta18(8) = '0' then corr18 <= resta18(7 downto 0); else corr18 <=  not resta18(7 downto 0); end if;
if resta19(8) = '0' then corr19 <= resta19(7 downto 0); else corr19 <=  not resta19(7 downto 0); end if;
if resta20(8) = '0' then corr20 <= resta20(7 downto 0); else corr20 <=  not resta20(7 downto 0); end if;
if resta21(8) = '0' then corr21 <= resta21(7 downto 0); else corr21 <=  not resta21(7 downto 0); end if;
if resta22(8) = '0' then corr22 <= resta22(7 downto 0); else corr22 <=  not resta22(7 downto 0); end if;
if resta23(8) = '0' then corr23 <= resta23(7 downto 0); else corr23 <=  not resta23(7 downto 0); end if;
if resta24(8) = '0' then corr24 <= resta24(7 downto 0); else corr24 <=  not resta24(7 downto 0); end if;
if resta25(8) = '0' then corr25 <= resta25(7 downto 0); else corr25 <=  not resta25(7 downto 0); end if;
if resta26(8) = '0' then corr26 <= resta26(7 downto 0); else corr26 <=  not resta26(7 downto 0); end if;
if resta27(8) = '0' then corr27 <= resta27(7 downto 0); else corr27 <=  not resta27(7 downto 0); end if;
if resta28(8) = '0' then corr28 <= resta28(7 downto 0); else corr28 <=  not resta28(7 downto 0); end if;
if resta29(8) = '0' then corr29 <= resta29(7 downto 0); else corr29 <=  not resta29(7 downto 0); end if;
if resta30(8) = '0' then corr30 <= resta30(7 downto 0); else corr30 <=  not resta30(7 downto 0); end if;
if resta31(8) = '0' then corr31 <= resta31(7 downto 0); else corr31 <=  not resta31(7 downto 0); end if;
if resta32(8) = '0' then corr32 <= resta32(7 downto 0); else corr32 <=  not resta32(7 downto 0); end if;
if resta33(8) = '0' then corr33 <= resta33(7 downto 0); else corr33 <=  not resta33(7 downto 0); end if;
if resta34(8) = '0' then corr34 <= resta34(7 downto 0); else corr34 <=  not resta34(7 downto 0); end if;
if resta35(8) = '0' then corr35 <= resta35(7 downto 0); else corr35 <=  not resta35(7 downto 0); end if;
if resta36(8) = '0' then corr36 <= resta36(7 downto 0); else corr36 <=  not resta36(7 downto 0); end if;
if resta37(8) = '0' then corr37 <= resta37(7 downto 0); else corr37 <=  not resta37(7 downto 0); end if;
if resta38(8) = '0' then corr38 <= resta38(7 downto 0); else corr38 <=  not resta38(7 downto 0); end if;
if resta39(8) = '0' then corr39 <= resta39(7 downto 0); else corr39 <=  not resta39(7 downto 0); end if;
if resta40(8) = '0' then corr40 <= resta40(7 downto 0); else corr40 <=  not resta40(7 downto 0); end if;
if resta41(8) = '0' then corr41 <= resta41(7 downto 0); else corr41 <=  not resta41(7 downto 0); end if;
if resta42(8) = '0' then corr42 <= resta42(7 downto 0); else corr42 <=  not resta42(7 downto 0); end if;
if resta43(8) = '0' then corr43 <= resta43(7 downto 0); else corr43 <=  not resta43(7 downto 0); end if;
if resta44(8) = '0' then corr44 <= resta44(7 downto 0); else corr44 <=  not resta44(7 downto 0); end if;
if resta45(8) = '0' then corr45 <= resta45(7 downto 0); else corr45 <=  not resta45(7 downto 0); end if;
if resta46(8) = '0' then corr46 <= resta46(7 downto 0); else corr46 <=  not resta46(7 downto 0); end if;
if resta47(8) = '0' then corr47 <= resta47(7 downto 0); else corr47 <=  not resta47(7 downto 0); end if;
if resta48(8) = '0' then corr48 <= resta48(7 downto 0); else corr48 <=  not resta48(7 downto 0); end if;
if resta49(8) = '0' then corr49 <= resta49(7 downto 0); else corr49 <=  not resta49(7 downto 0); end if;
if resta50(8) = '0' then corr50 <= resta50(7 downto 0); else corr50 <=  not resta50(7 downto 0); end if;
if resta51(8) = '0' then corr51 <= resta51(7 downto 0); else corr51 <=  not resta51(7 downto 0); end if;
if resta52(8) = '0' then corr52 <= resta52(7 downto 0); else corr52 <=  not resta52(7 downto 0); end if;
if resta53(8) = '0' then corr53 <= resta53(7 downto 0); else corr53 <=  not resta53(7 downto 0); end if;
if resta54(8) = '0' then corr54 <= resta54(7 downto 0); else corr54 <=  not resta54(7 downto 0); end if;
if resta55(8) = '0' then corr55 <= resta55(7 downto 0); else corr55 <=  not resta55(7 downto 0); end if;
if resta56(8) = '0' then corr56 <= resta56(7 downto 0); else corr56 <=  not resta56(7 downto 0); end if;
if resta57(8) = '0' then corr57 <= resta57(7 downto 0); else corr57 <=  not resta57(7 downto 0); end if;
if resta58(8) = '0' then corr58 <= resta58(7 downto 0); else corr58 <=  not resta58(7 downto 0); end if;
if resta59(8) = '0' then corr59 <= resta59(7 downto 0); else corr59 <=  not resta59(7 downto 0); end if;
if resta60(8) = '0' then corr60 <= resta60(7 downto 0); else corr60 <=  not resta60(7 downto 0); end if;
if resta61(8) = '0' then corr61 <= resta61(7 downto 0); else corr61 <=  not resta61(7 downto 0); end if;
if resta62(8) = '0' then corr62 <= resta62(7 downto 0); else corr62 <=  not resta62(7 downto 0); end if;
if resta63(8) = '0' then corr63 <= resta63(7 downto 0); else corr63 <=  not resta63(7 downto 0); end if;
if resta64(8) = '0' then corr64 <= resta64(7 downto 0); else corr64 <=  not resta64(7 downto 0); end if;
if resta65(8) = '0' then corr65 <= resta65(7 downto 0); else corr65 <=  not resta65(7 downto 0); end if;
if resta66(8) = '0' then corr66 <= resta66(7 downto 0); else corr66 <=  not resta66(7 downto 0); end if;
if resta67(8) = '0' then corr67 <= resta67(7 downto 0); else corr67 <=  not resta67(7 downto 0); end if;
if resta68(8) = '0' then corr68 <= resta68(7 downto 0); else corr68 <=  not resta68(7 downto 0); end if;
if resta69(8) = '0' then corr69 <= resta69(7 downto 0); else corr69 <=  not resta69(7 downto 0); end if;
if resta70(8) = '0' then corr70 <= resta70(7 downto 0); else corr70 <=  not resta70(7 downto 0); end if;
if resta71(8) = '0' then corr71 <= resta71(7 downto 0); else corr71 <=  not resta71(7 downto 0); end if;
if resta72(8) = '0' then corr72 <= resta72(7 downto 0); else corr72 <=  not resta72(7 downto 0); end if;
if resta73(8) = '0' then corr73 <= resta73(7 downto 0); else corr73 <=  not resta73(7 downto 0); end if;
if resta74(8) = '0' then corr74 <= resta74(7 downto 0); else corr74 <=  not resta74(7 downto 0); end if;
if resta75(8) = '0' then corr75 <= resta75(7 downto 0); else corr75 <=  not resta75(7 downto 0); end if;
if resta76(8) = '0' then corr76 <= resta76(7 downto 0); else corr76 <=  not resta76(7 downto 0); end if;
if resta77(8) = '0' then corr77 <= resta77(7 downto 0); else corr77 <=  not resta77(7 downto 0); end if;
if resta78(8) = '0' then corr78 <= resta78(7 downto 0); else corr78 <=  not resta78(7 downto 0); end if;
if resta79(8) = '0' then corr79 <= resta79(7 downto 0); else corr79 <=  not resta79(7 downto 0); end if;
if resta80(8) = '0' then corr80 <= resta80(7 downto 0); else corr80 <=  not resta80(7 downto 0); end if;
if resta81(8) = '0' then corr81 <= resta81(7 downto 0); else corr81 <=  not resta81(7 downto 0); end if;
if resta82(8) = '0' then corr82 <= resta82(7 downto 0); else corr82 <=  not resta82(7 downto 0); end if;
if resta83(8) = '0' then corr83 <= resta83(7 downto 0); else corr83 <=  not resta83(7 downto 0); end if;
if resta84(8) = '0' then corr84 <= resta84(7 downto 0); else corr84 <=  not resta84(7 downto 0); end if;
if resta85(8) = '0' then corr85 <= resta85(7 downto 0); else corr85 <=  not resta85(7 downto 0); end if;
if resta86(8) = '0' then corr86 <= resta86(7 downto 0); else corr86 <=  not resta86(7 downto 0); end if;
if resta87(8) = '0' then corr87 <= resta87(7 downto 0); else corr87 <=  not resta87(7 downto 0); end if;
if resta88(8) = '0' then corr88 <= resta88(7 downto 0); else corr88 <=  not resta88(7 downto 0); end if;
if resta89(8) = '0' then corr89 <= resta89(7 downto 0); else corr89 <=  not resta89(7 downto 0); end if;
if resta90(8) = '0' then corr90 <= resta90(7 downto 0); else corr90 <=  not resta90(7 downto 0); end if;
if resta91(8) = '0' then corr91 <= resta91(7 downto 0); else corr91 <=  not resta91(7 downto 0); end if;
if resta92(8) = '0' then corr92 <= resta92(7 downto 0); else corr92 <=  not resta92(7 downto 0); end if;
if resta93(8) = '0' then corr93 <= resta93(7 downto 0); else corr93 <=  not resta93(7 downto 0); end if;
if resta94(8) = '0' then corr94 <= resta94(7 downto 0); else corr94 <=  not resta94(7 downto 0); end if;
if resta95(8) = '0' then corr95 <= resta95(7 downto 0); else corr95 <=  not resta95(7 downto 0); end if;
if resta96(8) = '0' then corr96 <= resta96(7 downto 0); else corr96 <=  not resta96(7 downto 0); end if;
if resta97(8) = '0' then corr97 <= resta97(7 downto 0); else corr97 <=  not resta97(7 downto 0); end if;
if resta98(8) = '0' then corr98 <= resta98(7 downto 0); else corr98 <=  not resta98(7 downto 0); end if;
if resta99(8) = '0' then corr99 <= resta99(7 downto 0); else corr99 <=  not resta99(7 downto 0); end if;
if resta100(8) = '0' then corr100 <= resta100(7 downto 0); else corr100 <=  not resta100(7 downto 0); end if;
if resta101(8) = '0' then corr101 <= resta101(7 downto 0); else corr101 <=  not resta101(7 downto 0); end if;
if resta102(8) = '0' then corr102 <= resta102(7 downto 0); else corr102 <=  not resta102(7 downto 0); end if;
if resta103(8) = '0' then corr103 <= resta103(7 downto 0); else corr103 <=  not resta103(7 downto 0); end if;
if resta104(8) = '0' then corr104 <= resta104(7 downto 0); else corr104 <=  not resta104(7 downto 0); end if;
if resta105(8) = '0' then corr105 <= resta105(7 downto 0); else corr105 <=  not resta105(7 downto 0); end if;
if resta106(8) = '0' then corr106 <= resta106(7 downto 0); else corr106 <=  not resta106(7 downto 0); end if;
if resta107(8) = '0' then corr107 <= resta107(7 downto 0); else corr107 <=  not resta107(7 downto 0); end if;
if resta108(8) = '0' then corr108 <= resta108(7 downto 0); else corr108 <=  not resta108(7 downto 0); end if;
if resta109(8) = '0' then corr109 <= resta109(7 downto 0); else corr109 <=  not resta109(7 downto 0); end if;
if resta110(8) = '0' then corr110 <= resta110(7 downto 0); else corr110 <=  not resta110(7 downto 0); end if;
if resta111(8) = '0' then corr111 <= resta111(7 downto 0); else corr111 <=  not resta111(7 downto 0); end if;
if resta112(8) = '0' then corr112 <= resta112(7 downto 0); else corr112 <=  not resta112(7 downto 0); end if;
if resta113(8) = '0' then corr113 <= resta113(7 downto 0); else corr113 <=  not resta113(7 downto 0); end if;
if resta114(8) = '0' then corr114 <= resta114(7 downto 0); else corr114 <=  not resta114(7 downto 0); end if;
if resta115(8) = '0' then corr115 <= resta115(7 downto 0); else corr115 <=  not resta115(7 downto 0); end if;
if resta116(8) = '0' then corr116 <= resta116(7 downto 0); else corr116 <=  not resta116(7 downto 0); end if;
if resta117(8) = '0' then corr117 <= resta117(7 downto 0); else corr117 <=  not resta117(7 downto 0); end if;
if resta118(8) = '0' then corr118 <= resta118(7 downto 0); else corr118 <=  not resta118(7 downto 0); end if;
if resta119(8) = '0' then corr119 <= resta119(7 downto 0); else corr119 <=  not resta119(7 downto 0); end if;
if resta120(8) = '0' then corr120 <= resta120(7 downto 0); else corr120 <=  not resta120(7 downto 0); end if;
if resta121(8) = '0' then corr121 <= resta121(7 downto 0); else corr121 <=  not resta121(7 downto 0); end if;
if resta122(8) = '0' then corr122 <= resta122(7 downto 0); else corr122 <=  not resta122(7 downto 0); end if;
if resta123(8) = '0' then corr123 <= resta123(7 downto 0); else corr123 <=  not resta123(7 downto 0); end if;
if resta124(8) = '0' then corr124 <= resta124(7 downto 0); else corr124 <=  not resta124(7 downto 0); end if;
if resta125(8) = '0' then corr125 <= resta125(7 downto 0); else corr125 <=  not resta125(7 downto 0); end if;
if resta126(8) = '0' then corr126 <= resta126(7 downto 0); else corr126 <=  not resta126(7 downto 0); end if;
if resta127(8) = '0' then corr127 <= resta127(7 downto 0); else corr127 <=  not resta127(7 downto 0); end if;
if resta128(8) = '0' then corr128 <= resta128(7 downto 0); else corr128 <=  not resta128(7 downto 0); end if;
if resta129(8) = '0' then corr129 <= resta129(7 downto 0); else corr129 <=  not resta129(7 downto 0); end if;
if resta130(8) = '0' then corr130 <= resta130(7 downto 0); else corr130 <=  not resta130(7 downto 0); end if;
if resta131(8) = '0' then corr131 <= resta131(7 downto 0); else corr131 <=  not resta131(7 downto 0); end if;
if resta132(8) = '0' then corr132 <= resta132(7 downto 0); else corr132 <=  not resta132(7 downto 0); end if;
if resta133(8) = '0' then corr133 <= resta133(7 downto 0); else corr133 <=  not resta133(7 downto 0); end if;
if resta134(8) = '0' then corr134 <= resta134(7 downto 0); else corr134 <=  not resta134(7 downto 0); end if;
if resta135(8) = '0' then corr135 <= resta135(7 downto 0); else corr135 <=  not resta135(7 downto 0); end if;
if resta136(8) = '0' then corr136 <= resta136(7 downto 0); else corr136 <=  not resta136(7 downto 0); end if;
if resta137(8) = '0' then corr137 <= resta137(7 downto 0); else corr137 <=  not resta137(7 downto 0); end if;
if resta138(8) = '0' then corr138 <= resta138(7 downto 0); else corr138 <=  not resta138(7 downto 0); end if;
if resta139(8) = '0' then corr139 <= resta139(7 downto 0); else corr139 <=  not resta139(7 downto 0); end if;
if resta140(8) = '0' then corr140 <= resta140(7 downto 0); else corr140 <=  not resta140(7 downto 0); end if;
if resta141(8) = '0' then corr141 <= resta141(7 downto 0); else corr141 <=  not resta141(7 downto 0); end if;
if resta142(8) = '0' then corr142 <= resta142(7 downto 0); else corr142 <=  not resta142(7 downto 0); end if;
if resta143(8) = '0' then corr143 <= resta143(7 downto 0); else corr143 <=  not resta143(7 downto 0); end if;
if resta144(8) = '0' then corr144 <= resta144(7 downto 0); else corr144 <=  not resta144(7 downto 0); end if;
if resta145(8) = '0' then corr145 <= resta145(7 downto 0); else corr145 <=  not resta145(7 downto 0); end if;
if resta146(8) = '0' then corr146 <= resta146(7 downto 0); else corr146 <=  not resta146(7 downto 0); end if;
if resta147(8) = '0' then corr147 <= resta147(7 downto 0); else corr147 <=  not resta147(7 downto 0); end if;
if resta148(8) = '0' then corr148 <= resta148(7 downto 0); else corr148 <=  not resta148(7 downto 0); end if;
if resta149(8) = '0' then corr149 <= resta149(7 downto 0); else corr149 <=  not resta149(7 downto 0); end if;
if resta150(8) = '0' then corr150 <= resta150(7 downto 0); else corr150 <=  not resta150(7 downto 0); end if;
if resta151(8) = '0' then corr151 <= resta151(7 downto 0); else corr151 <=  not resta151(7 downto 0); end if;
if resta152(8) = '0' then corr152 <= resta152(7 downto 0); else corr152 <=  not resta152(7 downto 0); end if;
if resta153(8) = '0' then corr153 <= resta153(7 downto 0); else corr153 <=  not resta153(7 downto 0); end if;
if resta154(8) = '0' then corr154 <= resta154(7 downto 0); else corr154 <=  not resta154(7 downto 0); end if;
if resta155(8) = '0' then corr155 <= resta155(7 downto 0); else corr155 <=  not resta155(7 downto 0); end if;
if resta156(8) = '0' then corr156 <= resta156(7 downto 0); else corr156 <=  not resta156(7 downto 0); end if;
if resta157(8) = '0' then corr157 <= resta157(7 downto 0); else corr157 <=  not resta157(7 downto 0); end if;
if resta158(8) = '0' then corr158 <= resta158(7 downto 0); else corr158 <=  not resta158(7 downto 0); end if;
if resta159(8) = '0' then corr159 <= resta159(7 downto 0); else corr159 <=  not resta159(7 downto 0); end if;
if resta160(8) = '0' then corr160 <= resta160(7 downto 0); else corr160 <=  not resta160(7 downto 0); end if;
if resta161(8) = '0' then corr161 <= resta161(7 downto 0); else corr161 <=  not resta161(7 downto 0); end if;
if resta162(8) = '0' then corr162 <= resta162(7 downto 0); else corr162 <=  not resta162(7 downto 0); end if;
if resta163(8) = '0' then corr163 <= resta163(7 downto 0); else corr163 <=  not resta163(7 downto 0); end if;
if resta164(8) = '0' then corr164 <= resta164(7 downto 0); else corr164 <=  not resta164(7 downto 0); end if;
if resta165(8) = '0' then corr165 <= resta165(7 downto 0); else corr165 <=  not resta165(7 downto 0); end if;
if resta166(8) = '0' then corr166 <= resta166(7 downto 0); else corr166 <=  not resta166(7 downto 0); end if;
if resta167(8) = '0' then corr167 <= resta167(7 downto 0); else corr167 <=  not resta167(7 downto 0); end if;
if resta168(8) = '0' then corr168 <= resta168(7 downto 0); else corr168 <=  not resta168(7 downto 0); end if;
if resta169(8) = '0' then corr169 <= resta169(7 downto 0); else corr169 <=  not resta169(7 downto 0); end if;
if resta170(8) = '0' then corr170 <= resta170(7 downto 0); else corr170 <=  not resta170(7 downto 0); end if;
if resta171(8) = '0' then corr171 <= resta171(7 downto 0); else corr171 <=  not resta171(7 downto 0); end if;
if resta172(8) = '0' then corr172 <= resta172(7 downto 0); else corr172 <=  not resta172(7 downto 0); end if;
if resta173(8) = '0' then corr173 <= resta173(7 downto 0); else corr173 <=  not resta173(7 downto 0); end if;
if resta174(8) = '0' then corr174 <= resta174(7 downto 0); else corr174 <=  not resta174(7 downto 0); end if;
if resta175(8) = '0' then corr175 <= resta175(7 downto 0); else corr175 <=  not resta175(7 downto 0); end if;
if resta176(8) = '0' then corr176 <= resta176(7 downto 0); else corr176 <=  not resta176(7 downto 0); end if;
if resta177(8) = '0' then corr177 <= resta177(7 downto 0); else corr177 <=  not resta177(7 downto 0); end if;
if resta178(8) = '0' then corr178 <= resta178(7 downto 0); else corr178 <=  not resta178(7 downto 0); end if;
if resta179(8) = '0' then corr179 <= resta179(7 downto 0); else corr179 <=  not resta179(7 downto 0); end if;
if resta180(8) = '0' then corr180 <= resta180(7 downto 0); else corr180 <=  not resta180(7 downto 0); end if;
if resta181(8) = '0' then corr181 <= resta181(7 downto 0); else corr181 <=  not resta181(7 downto 0); end if;
if resta182(8) = '0' then corr182 <= resta182(7 downto 0); else corr182 <=  not resta182(7 downto 0); end if;
if resta183(8) = '0' then corr183 <= resta183(7 downto 0); else corr183 <=  not resta183(7 downto 0); end if;
if resta184(8) = '0' then corr184 <= resta184(7 downto 0); else corr184 <=  not resta184(7 downto 0); end if;
if resta185(8) = '0' then corr185 <= resta185(7 downto 0); else corr185 <=  not resta185(7 downto 0); end if;
if resta186(8) = '0' then corr186 <= resta186(7 downto 0); else corr186 <=  not resta186(7 downto 0); end if;
if resta187(8) = '0' then corr187 <= resta187(7 downto 0); else corr187 <=  not resta187(7 downto 0); end if;
if resta188(8) = '0' then corr188 <= resta188(7 downto 0); else corr188 <=  not resta188(7 downto 0); end if;
if resta189(8) = '0' then corr189 <= resta189(7 downto 0); else corr189 <=  not resta189(7 downto 0); end if;
if resta190(8) = '0' then corr190 <= resta190(7 downto 0); else corr190 <=  not resta190(7 downto 0); end if;
if resta191(8) = '0' then corr191 <= resta191(7 downto 0); else corr191 <=  not resta191(7 downto 0); end if;
if resta192(8) = '0' then corr192 <= resta192(7 downto 0); else corr192 <=  not resta192(7 downto 0); end if;
if resta193(8) = '0' then corr193 <= resta193(7 downto 0); else corr193 <=  not resta193(7 downto 0); end if;
if resta194(8) = '0' then corr194 <= resta194(7 downto 0); else corr194 <=  not resta194(7 downto 0); end if;
if resta195(8) = '0' then corr195 <= resta195(7 downto 0); else corr195 <=  not resta195(7 downto 0); end if;
if resta196(8) = '0' then corr196 <= resta196(7 downto 0); else corr196 <=  not resta196(7 downto 0); end if;
if resta197(8) = '0' then corr197 <= resta197(7 downto 0); else corr197 <=  not resta197(7 downto 0); end if;
if resta198(8) = '0' then corr198 <= resta198(7 downto 0); else corr198 <=  not resta198(7 downto 0); end if;
if resta199(8) = '0' then corr199 <= resta199(7 downto 0); else corr199 <=  not resta199(7 downto 0); end if;
if resta200(8) = '0' then corr200 <= resta200(7 downto 0); else corr200 <=  not resta200(7 downto 0); end if;
if resta201(8) = '0' then corr201 <= resta201(7 downto 0); else corr201 <=  not resta201(7 downto 0); end if;
if resta202(8) = '0' then corr202 <= resta202(7 downto 0); else corr202 <=  not resta202(7 downto 0); end if;
if resta203(8) = '0' then corr203 <= resta203(7 downto 0); else corr203 <=  not resta203(7 downto 0); end if;
if resta204(8) = '0' then corr204 <= resta204(7 downto 0); else corr204 <=  not resta204(7 downto 0); end if;
if resta205(8) = '0' then corr205 <= resta205(7 downto 0); else corr205 <=  not resta205(7 downto 0); end if;
if resta206(8) = '0' then corr206 <= resta206(7 downto 0); else corr206 <=  not resta206(7 downto 0); end if;
if resta207(8) = '0' then corr207 <= resta207(7 downto 0); else corr207 <=  not resta207(7 downto 0); end if;
if resta208(8) = '0' then corr208 <= resta208(7 downto 0); else corr208 <=  not resta208(7 downto 0); end if;
if resta209(8) = '0' then corr209 <= resta209(7 downto 0); else corr209 <=  not resta209(7 downto 0); end if;
if resta210(8) = '0' then corr210 <= resta210(7 downto 0); else corr210 <=  not resta210(7 downto 0); end if;
if resta211(8) = '0' then corr211 <= resta211(7 downto 0); else corr211 <=  not resta211(7 downto 0); end if;
if resta212(8) = '0' then corr212 <= resta212(7 downto 0); else corr212 <=  not resta212(7 downto 0); end if;
if resta213(8) = '0' then corr213 <= resta213(7 downto 0); else corr213 <=  not resta213(7 downto 0); end if;
if resta214(8) = '0' then corr214 <= resta214(7 downto 0); else corr214 <=  not resta214(7 downto 0); end if;
if resta215(8) = '0' then corr215 <= resta215(7 downto 0); else corr215 <=  not resta215(7 downto 0); end if;
if resta216(8) = '0' then corr216 <= resta216(7 downto 0); else corr216 <=  not resta216(7 downto 0); end if;
if resta217(8) = '0' then corr217 <= resta217(7 downto 0); else corr217 <=  not resta217(7 downto 0); end if;
if resta218(8) = '0' then corr218 <= resta218(7 downto 0); else corr218 <=  not resta218(7 downto 0); end if;
if resta219(8) = '0' then corr219 <= resta219(7 downto 0); else corr219 <=  not resta219(7 downto 0); end if;
if resta220(8) = '0' then corr220 <= resta220(7 downto 0); else corr220 <=  not resta220(7 downto 0); end if;
if resta221(8) = '0' then corr221 <= resta221(7 downto 0); else corr221 <=  not resta221(7 downto 0); end if;
if resta222(8) = '0' then corr222 <= resta222(7 downto 0); else corr222 <=  not resta222(7 downto 0); end if;
if resta223(8) = '0' then corr223 <= resta223(7 downto 0); else corr223 <=  not resta223(7 downto 0); end if;
if resta224(8) = '0' then corr224 <= resta224(7 downto 0); else corr224 <=  not resta224(7 downto 0); end if;
if resta225(8) = '0' then corr225 <= resta225(7 downto 0); else corr225 <=  not resta225(7 downto 0); end if;
if resta226(8) = '0' then corr226 <= resta226(7 downto 0); else corr226 <=  not resta226(7 downto 0); end if;
if resta227(8) = '0' then corr227 <= resta227(7 downto 0); else corr227 <=  not resta227(7 downto 0); end if;
if resta228(8) = '0' then corr228 <= resta228(7 downto 0); else corr228 <=  not resta228(7 downto 0); end if;
if resta229(8) = '0' then corr229 <= resta229(7 downto 0); else corr229 <=  not resta229(7 downto 0); end if;
if resta230(8) = '0' then corr230 <= resta230(7 downto 0); else corr230 <=  not resta230(7 downto 0); end if;
if resta231(8) = '0' then corr231 <= resta231(7 downto 0); else corr231 <=  not resta231(7 downto 0); end if;
if resta232(8) = '0' then corr232 <= resta232(7 downto 0); else corr232 <=  not resta232(7 downto 0); end if;
if resta233(8) = '0' then corr233 <= resta233(7 downto 0); else corr233 <=  not resta233(7 downto 0); end if;
if resta234(8) = '0' then corr234 <= resta234(7 downto 0); else corr234 <=  not resta234(7 downto 0); end if;
if resta235(8) = '0' then corr235 <= resta235(7 downto 0); else corr235 <=  not resta235(7 downto 0); end if;
if resta236(8) = '0' then corr236 <= resta236(7 downto 0); else corr236 <=  not resta236(7 downto 0); end if;
if resta237(8) = '0' then corr237 <= resta237(7 downto 0); else corr237 <=  not resta237(7 downto 0); end if;
if resta238(8) = '0' then corr238 <= resta238(7 downto 0); else corr238 <=  not resta238(7 downto 0); end if;
if resta239(8) = '0' then corr239 <= resta239(7 downto 0); else corr239 <=  not resta239(7 downto 0); end if;
if resta240(8) = '0' then corr240 <= resta240(7 downto 0); else corr240 <=  not resta240(7 downto 0); end if;
if resta241(8) = '0' then corr241 <= resta241(7 downto 0); else corr241 <=  not resta241(7 downto 0); end if;
if resta242(8) = '0' then corr242 <= resta242(7 downto 0); else corr242 <=  not resta242(7 downto 0); end if;
if resta243(8) = '0' then corr243 <= resta243(7 downto 0); else corr243 <=  not resta243(7 downto 0); end if;
if resta244(8) = '0' then corr244 <= resta244(7 downto 0); else corr244 <=  not resta244(7 downto 0); end if;
if resta245(8) = '0' then corr245 <= resta245(7 downto 0); else corr245 <=  not resta245(7 downto 0); end if;
if resta246(8) = '0' then corr246 <= resta246(7 downto 0); else corr246 <=  not resta246(7 downto 0); end if;
if resta247(8) = '0' then corr247 <= resta247(7 downto 0); else corr247 <=  not resta247(7 downto 0); end if;
if resta248(8) = '0' then corr248 <= resta248(7 downto 0); else corr248 <=  not resta248(7 downto 0); end if;
if resta249(8) = '0' then corr249 <= resta249(7 downto 0); else corr249 <=  not resta249(7 downto 0); end if;
if resta250(8) = '0' then corr250 <= resta250(7 downto 0); else corr250 <=  not resta250(7 downto 0); end if;
if resta251(8) = '0' then corr251 <= resta251(7 downto 0); else corr251 <=  not resta251(7 downto 0); end if;
if resta252(8) = '0' then corr252 <= resta252(7 downto 0); else corr252 <=  not resta252(7 downto 0); end if;
if resta253(8) = '0' then corr253 <= resta253(7 downto 0); else corr253 <=  not resta253(7 downto 0); end if;
if resta254(8) = '0' then corr254 <= resta254(7 downto 0); else corr254 <=  not resta254(7 downto 0); end if;
if resta255(8) = '0' then corr255 <= resta255(7 downto 0); else corr255 <=  not resta255(7 downto 0); end if;
      
regSignos00 <= signos00;	regSignos01 <= signos01;	regSignos02 <= signos02;	regSignos03 <= signos03;	regSignos04 <= signos04;	regSignos05 <= signos05;	regSignos06 <= signos06;	regSignos07 <= signos07;	regSignos08 <= signos08;	regSignos09 <= signos09;	regSignos10 <= signos10;	regSignos11 <= signos11;	regSignos12 <= signos12;	regSignos13 <= signos13;	regSignos14 <= signos14;	regSignos15 <= signos15;	regSignos16 <= signos16;	regSignos17 <= signos17;	regSignos18 <= signos18;	regSignos19 <= signos19;	regSignos20 <= signos20;	regSignos21 <= signos21;	regSignos22 <= signos22;	regSignos23 <= signos23;	regSignos24 <= signos24;	regSignos25 <= signos25;	regSignos26 <= signos26;	regSignos27 <= signos27;	regSignos28 <= signos28;	regSignos29 <= signos29;	regSignos30 <= signos30;	regSignos31 <= signos31;	regSignos32 <= signos32;	regSignos33 <= signos33;	regSignos34 <= signos34;	regSignos35 <= signos35;	regSignos36 <= signos36;	regSignos37 <= signos37;	regSignos38 <= signos38;	regSignos39 <= signos39;	regSignos40 <= signos40;	regSignos41 <= signos41;	regSignos42 <= signos42;        
        
        
sigOne0 <= ("00" & regSignos00) + ("00" & regSignos01) + ("00" & regSignos02) + ("00" & regSignos03);
sigOne1 <= ("00" & regSignos04) + ("00" & regSignos05) + ("00" & regSignos06) + ("00" & regSignos07);
sigOne2 <= ("00" & regSignos08) + ("00" & regSignos09) + ("00" & regSignos10) + ("00" & regSignos11);
sigOne3 <= ("00" & regSignos12) + ("00" & regSignos13) + ("00" & regSignos14) + ("00" & regSignos15);
sigOne4 <= ("00" & regSignos16) + ("00" & regSignos17) + ("00" & regSignos18) + ("00" & regSignos19);
sigOne5 <= ("00" & regSignos20) + ("00" & regSignos21) + ("00" & regSignos22) + ("00" & regSignos23);
sigOne6 <= ("00" & regSignos24) + ("00" & regSignos25) + ("00" & regSignos26) + ("00" & regSignos27);
sigOne7 <= ("00" & regSignos28) + ("00" & regSignos29) + ("00" & regSignos30) + ("00" & regSignos31);
sigOne8 <= ("00" & regSignos32) + ("00" & regSignos33) + ("00" & regSignos34) + ("00" & regSignos35);
sigOne9 <= ("00" & regSignos36) + ("00" & regSignos37) + ("00" & regSignos38) + ("00" & regSignos39);
sigOne10 <= ("00" & regSignos40) + ("00" & regSignos41) + ("00" & regSignos42);

       
        
one0 <= ("00" & corr0) + ("00" & corr1) + ("00" & corr2) + ("00" & corr3);
one1 <= ("00" & corr4) + ("00" & corr5) + ("00" & corr6) + ("00" & corr7);
one2 <= ("00" & corr8) + ("00" & corr9) + ("00" & corr10) + ("00" & corr11);
one3 <= ("00" & corr12) + ("00" & corr13) + ("00" & corr14) + ("00" & corr15);
one4 <= ("00" & corr16) + ("00" & corr17) + ("00" & corr18) + ("00" & corr19);
one5 <= ("00" & corr20) + ("00" & corr21) + ("00" & corr22) + ("00" & corr23);
one6 <= ("00" & corr24) + ("00" & corr25) + ("00" & corr26) + ("00" & corr27);
one7 <= ("00" & corr28) + ("00" & corr29) + ("00" & corr30) + ("00" & corr31);
one8 <= ("00" & corr32) + ("00" & corr33) + ("00" & corr34) + ("00" & corr35);
one9 <= ("00" & corr36) + ("00" & corr37) + ("00" & corr38) + ("00" & corr39);
one10 <= ("00" & corr40) + ("00" & corr41) + ("00" & corr42) + ("00" & corr43);
one11 <= ("00" & corr44) + ("00" & corr45) + ("00" & corr46) + ("00" & corr47);
one12 <= ("00" & corr48) + ("00" & corr49) + ("00" & corr50) + ("00" & corr51);
one13 <= ("00" & corr52) + ("00" & corr53) + ("00" & corr54) + ("00" & corr55);
one14 <= ("00" & corr56) + ("00" & corr57) + ("00" & corr58) + ("00" & corr59);
one15 <= ("00" & corr60) + ("00" & corr61) + ("00" & corr62) + ("00" & corr63);
one16 <= ("00" & corr64) + ("00" & corr65) + ("00" & corr66) + ("00" & corr67);
one17 <= ("00" & corr68) + ("00" & corr69) + ("00" & corr70) + ("00" & corr71);
one18 <= ("00" & corr72) + ("00" & corr73) + ("00" & corr74) + ("00" & corr75);
one19 <= ("00" & corr76) + ("00" & corr77) + ("00" & corr78) + ("00" & corr79);
one20 <= ("00" & corr80) + ("00" & corr81) + ("00" & corr82) + ("00" & corr83);
one21 <= ("00" & corr84) + ("00" & corr85) + ("00" & corr86) + ("00" & corr87);
one22 <= ("00" & corr88) + ("00" & corr89) + ("00" & corr90) + ("00" & corr91);
one23 <= ("00" & corr92) + ("00" & corr93) + ("00" & corr94) + ("00" & corr95);
one24 <= ("00" & corr96) + ("00" & corr97) + ("00" & corr98) + ("00" & corr99);
one25 <= ("00" & corr100) + ("00" & corr101) + ("00" & corr102) + ("00" & corr103);
one26 <= ("00" & corr104) + ("00" & corr105) + ("00" & corr106) + ("00" & corr107);
one27 <= ("00" & corr108) + ("00" & corr109) + ("00" & corr110) + ("00" & corr111);
one28 <= ("00" & corr112) + ("00" & corr113) + ("00" & corr114) + ("00" & corr115);
one29 <= ("00" & corr116) + ("00" & corr117) + ("00" & corr118) + ("00" & corr119);
one30 <= ("00" & corr120) + ("00" & corr121) + ("00" & corr122) + ("00" & corr123);
one31 <= ("00" & corr124) + ("00" & corr125) + ("00" & corr126) + ("00" & corr127);
one32 <= ("00" & corr128) + ("00" & corr129) + ("00" & corr130) + ("00" & corr131);
one33 <= ("00" & corr132) + ("00" & corr133) + ("00" & corr134) + ("00" & corr135);
one34 <= ("00" & corr136) + ("00" & corr137) + ("00" & corr138) + ("00" & corr139);
one35 <= ("00" & corr140) + ("00" & corr141) + ("00" & corr142) + ("00" & corr143);
one36 <= ("00" & corr144) + ("00" & corr145) + ("00" & corr146) + ("00" & corr147);
one37 <= ("00" & corr148) + ("00" & corr149) + ("00" & corr150) + ("00" & corr151);
one38 <= ("00" & corr152) + ("00" & corr153) + ("00" & corr154) + ("00" & corr155);
one39 <= ("00" & corr156) + ("00" & corr157) + ("00" & corr158) + ("00" & corr159);
one40 <= ("00" & corr160) + ("00" & corr161) + ("00" & corr162) + ("00" & corr163);
one41 <= ("00" & corr164) + ("00" & corr165) + ("00" & corr166) + ("00" & corr167);
one42 <= ("00" & corr168) + ("00" & corr169) + ("00" & corr170) + ("00" & corr171);
one43 <= ("00" & corr172) + ("00" & corr173) + ("00" & corr174) + ("00" & corr175);
one44 <= ("00" & corr176) + ("00" & corr177) + ("00" & corr178) + ("00" & corr179);
one45 <= ("00" & corr180) + ("00" & corr181) + ("00" & corr182) + ("00" & corr183);
one46 <= ("00" & corr184) + ("00" & corr185) + ("00" & corr186) + ("00" & corr187);
one47 <= ("00" & corr188) + ("00" & corr189) + ("00" & corr190) + ("00" & corr191);
one48 <= ("00" & corr192) + ("00" & corr193) + ("00" & corr194) + ("00" & corr195);
one49 <= ("00" & corr196) + ("00" & corr197) + ("00" & corr198) + ("00" & corr199);
one50 <= ("00" & corr200) + ("00" & corr201) + ("00" & corr202) + ("00" & corr203);
one51 <= ("00" & corr204) + ("00" & corr205) + ("00" & corr206) + ("00" & corr207);
one52 <= ("00" & corr208) + ("00" & corr209) + ("00" & corr210) + ("00" & corr211);
one53 <= ("00" & corr212) + ("00" & corr213) + ("00" & corr214) + ("00" & corr215);
one54 <= ("00" & corr216) + ("00" & corr217) + ("00" & corr218) + ("00" & corr219);
one55 <= ("00" & corr220) + ("00" & corr221) + ("00" & corr222) + ("00" & corr223);
one56 <= ("00" & corr224) + ("00" & corr225) + ("00" & corr226) + ("00" & corr227);
one57 <= ("00" & corr228) + ("00" & corr229) + ("00" & corr230) + ("00" & corr231);
one58 <= ("00" & corr232) + ("00" & corr233) + ("00" & corr234) + ("00" & corr235);
one59 <= ("00" & corr236) + ("00" & corr237) + ("00" & corr238) + ("00" & corr239);
one60 <= ("00" & corr240) + ("00" & corr241) + ("00" & corr242) + ("00" & corr243);
one61 <= ("00" & corr244) + ("00" & corr245) + ("00" & corr246) + ("00" & corr247);
one62 <= ("00" & corr248) + ("00" & corr249) + ("00" & corr250) + ("00" & corr251);
one63 <= ("00" & corr252) + ("00" & corr253) + ("00" & corr254) + ("00" & corr255);        
        
two0 <= ("00" & one0) + ("00" & one1) + ("00" & one2) + ("00" & one3);
two1 <= ("00" & one4) + ("00" & one5) + ("00" & one6) + ("00" & one7);
two2 <= ("00" & one8) + ("00" & one9) + ("00" & one10) + ("00" & one11);
two3 <= ("00" & one12) + ("00" & one13) + ("00" & one14) + ("00" & one15);
two4 <= ("00" & one16) + ("00" & one17) + ("00" & one18) + ("00" & one19);
two5 <= ("00" & one20) + ("00" & one21) + ("00" & one22) + ("00" & one23);
two6 <= ("00" & one24) + ("00" & one25) + ("00" & one26) + ("00" & one27);
two7 <= ("00" & one28) + ("00" & one29) + ("00" & one30) + ("00" & one31);
two8 <= ("00" & one32) + ("00" & one33) + ("00" & one34) + ("00" & one35);
two9 <= ("00" & one36) + ("00" & one37) + ("00" & one38) + ("00" & one39);
two10 <= ("00" & one40) + ("00" & one41) + ("00" & one42) + ("00" & one43);
two11 <= ("00" & one44) + ("00" & one45) + ("00" & one46) + ("00" & one47);
two12 <= ("00" & one48) + ("00" & one49) + ("00" & one50) + ("00" & one51);
two13 <= ("00" & one52) + ("00" & one53) + ("00" & one54) + ("00" & one55);
two14 <= ("00" & one56) + ("00" & one57) + ("00" & one58) + ("00" & one59);
two15 <= ("00" & one60) + ("00" & one61) + ("00" & one62) + ("00" & one63);        
        
three0 <= ("00" & two0) + ("00" & two1) + ("00" & two2) + ("00" & two3);
three1 <= ("00" & two4) + ("00" & two5) + ("00" & two6) + ("00" & two7);
three2 <= ("00" & two8) + ("00" & two9) + ("00" & two10) + ("00" & two11);
three3 <= ("00" & two12) + ("00" & two13) + ("00" & two14) + ("00" & two15);

sigTwo0 <= ("00" & sigOne0) + ("00" & sigOne1) + ("00" & sigOne2) + ("00" & sigOne3);
sigTwo1 <= ("00" & sigOne4) + ("00" & sigOne5) + ("00" & sigOne6) + ("00" & sigOne7);
sigTwo2 <= ("00" & sigOne8) + ("00" & sigOne9) + ("00" & sigOne10);

sigThree <= ("00" & sigTwo0) + sigTwo1 + sigTwo2;


        case shift is
        when "00" =>        -- no hace nada
            act <= act; 
            ref <= ref; 
        when "01" =>        -- carga ambos arrays
            act(2047 downto 32) <= act(2015 downto 0); 
            act(31 downto 0) <= ref(17663 downto 17632); 
            ref(17663 downto 32) <= ref(17631 downto 0);
            ref(31 downto 0) <= dataIn;         
        when "10" =>        -- shift de 1 pixel (8 bits) de REF
            act <= act; 
            ref <= ref(17655 downto 0) & x"00";  -- esto ser�a rotaci�n y ahora no me interesa ref(17663 downto 17656); 
        when others =>      -- shift de cambio de l�nea 
            -- Tras el vector horizontal +15, hay: 
            -- 16 p�xeles que ya hemos usado, 
            -- 2 p�xeles extra que nunca se usan (48-46) 
            -- As� que hay que desplazar 18 p�xeles (144) para tener todo alienado   
            act <= act; 
            ref <= ref(17519 downto 0) & x"000000000000000000000000000000000000";
            --16511
        end case;  

        
    end if; 

end process; 

end Behavioral;
