/************************************************************************
 *  ADAPTADO DE: 
 * 
 *  mot_est.c, part of tmn (TMN encoder)
 *  Copyright (C) 1996  Telenor R&D, Norway
 *        Karl Olav Lillevold <Karl.Lillevold@nta.no>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Karl Olav Lillevold               <Karl.Lillevold@nta.no>
 *  Telenor Research and Development
 *  P.O.Box 83                        tel.:   +47 63 84 84 00
 *  N-2007 Kjeller, Norway            fax.:   +47 63 81 00 76
 *
 *  Robert Danielsen                  e-mail: Robert.Danielsen@nta.no
 *  Telenor Research and Development  www:    http://www.nta.no/brukere/DVC/
 *  P.O.Box 83                        tel.:   +47 63 84 84 00
 *  N-2007 Kjeller, Norway            fax.:   +47 63 81 00 76
 *
 *
 ************************************************************************/

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>
#include <sys/time.h>
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#define CL_TARGET_OPENCL_VERSION 120
#include <CL/cl.h>

#define AOCL_ALIGNMENT 64

#define _DEBUG_
#ifdef _DEBUG_
#define OPENCL_CHECK( operation )                               \
	err = operation;                                              \
	if (err != CL_SUCCESS)                                        \
		fprintf(stderr, "[OpenCL error]: Error number %d @%s:%d\n", \
			err, __FILE__, __LINE__);
#define OPENCL_CHECK_ERROR( err )                               \
	if (err != CL_SUCCESS)                                        \
		fprintf(stderr, "[OpenCL error]: Error number %d @%s:%d\n", \
			err, __FILE__, __LINE__);
#define MALLOC_CHECK( ptr )                                     \
	if (ptr == NULL)                                              \
		fprintf(stderr, "[malloc error]: Couldn't allocate buffer " \
			"\"%s\" @%s:%d\n", #ptr, __FILE__, __LINE__);
#else // !_DEBUG_
#define OPENCL_CHECK( operation ) \
	operation
#define OPENCL_CHECK_ERROR( err )
#define MALLOC_CHECK( ptr )
#endif // _DEBUG_

static inline double wtime(void)
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec + 1.0e-6 * tv.tv_usec;
}

typedef unsigned char byte;
typedef char int8;

int main(int argc, const char *argv[])
{
	if (argc != 3)
	{
		fprintf(stderr, "Usage: %s <filename> <repetitions>\n", argv[0]);
		fprintf(stderr, " - filename must be a file containing 2 raw B&W full HD video frames.\n");
		fprintf(stderr, " - repetitions is the number of times to compute motion estimation over the 2 frames "
				"(for performance testing purposes). It should be a multiple of 2.\n");
		exit(EXIT_FAILURE);
	}

	int REP = atoi(argv[2]);

	/* 0: Load video frames, setup result buffers */
	byte *frame1, *frame2, *curr, *prev;

	/* 0.1: allocate frame buffers */
	if (posix_memalign((void **)&frame1, AOCL_ALIGNMENT, (1920 + 30) * (1088 + 30) * sizeof(byte)) != 0)
	// The DMA engine requires host buffers to be aligned - https://www.mouser.com/pdfDocs/ug1352-get-moving-with-alveo.pdf
	//  (I know the link is about Xilinx FPGAs, I just couldn't find a good enough source to also back it up for Intel.)
	{
		fprintf(stderr, "ERROR - Couldn't allocate aligned memory for the first frame.\n");
		exit(EXIT_FAILURE);
	}

	if (posix_memalign((void **)&frame2, AOCL_ALIGNMENT, (1920 + 30) * (1088 + 30) * sizeof(byte)) != 0)
	{
		fprintf(stderr, "ERROR - Couldn't allocate memory for the second frame.\n");
		free(frame1);
		exit(EXIT_FAILURE);
	}

	curr = frame1, prev = frame2;

	/* 0.2: Read frames from file */
	FILE *frames = fopen(argv[1], "rb");
	if (!frames)
	{
		fprintf(stderr, "ERROR - Couldn't find or open video file: %s\n", argv[1]);
		exit(EXIT_FAILURE);
	}

	fread(frame1, 1920 + 30, 1088 + 30, frames);
	fread(frame2, 1920 + 30, 1088 + 30, frames);

	fclose(frames);

	/* 0.3: Allocate result buffers */
	const int results_per_frame = (1920 / 16) * (1080 / 16 + 1);
	int n_results = (1 + (REP > 1)) * results_per_frame;

	unsigned short *SADs;
	int8 *x_vectors, *y_vectors;

	if (posix_memalign((void **)&SADs, AOCL_ALIGNMENT, n_results * sizeof(unsigned short)) != 0)
	{
		fprintf(stderr, "ERROR - Couldn't allocate aligned memory for the SAD results.\n");
		free(curr);
		free(prev);
		exit(EXIT_FAILURE);
	}

	if (posix_memalign((void **)&x_vectors, AOCL_ALIGNMENT, n_results * sizeof(int8)) != 0)
	{
		fprintf(stderr, "ERROR - Couldn't allocate memory for the x vector results.\n");
		free(curr);
		free(prev);
		free(SADs);
		exit(EXIT_FAILURE);
	}

	if (posix_memalign((void **)&y_vectors, AOCL_ALIGNMENT, n_results * sizeof(int8)) != 0)
	{
		fprintf(stderr, "ERROR - Couldn't allocate memory for the y vector results.\n");
		free(curr);
		free(prev);
		free(SADs);
		free(x_vectors);
		exit(EXIT_FAILURE);
	}

	/* 1: OpenCL context initialization */
	cl_int err;

	cl_platform_id platform_id;
	cl_device_id device_id;

	cl_context context;
	cl_program program;
	cl_kernel kernel_ME;

	cl_command_queue_properties properties;
	cl_command_queue queue;

	cl_mem d_curr, d_prev, tmp, d_SAD_output, d_x_vector_output, d_y_vector_output;

	/* 1.1: Retrieve available platforms */
	cl_uint n_platforms;
	OPENCL_CHECK( clGetPlatformIDs(0, NULL, &n_platforms) );
	cl_platform_id *p_platforms = (cl_platform_id *)malloc(n_platforms * sizeof(cl_platform_id));
	MALLOC_CHECK( p_platforms );
	OPENCL_CHECK( clGetPlatformIDs(n_platforms, p_platforms, NULL) );

	/* 1.2: Get platform names and choose accordingly */
	char *platform_name;

	int cl_platform_index = -1;
	for (int i = 0; i < (int)n_platforms; i++)
	{
		size_t platform_name_size;
		OPENCL_CHECK( clGetPlatformInfo(p_platforms[i], CL_PLATFORM_NAME, 0, NULL, &platform_name_size) );
		platform_name = (char *)malloc(platform_name_size * sizeof(char));
		MALLOC_CHECK( p_platforms );

		OPENCL_CHECK( clGetPlatformInfo(p_platforms[i], CL_PLATFORM_NAME, platform_name_size, platform_name, NULL) );
		printf("[INFO] Found platform %d: %s\n", i, platform_name);
		#ifdef EMULATION
		if (!strncmp(platform_name, "Intel(R) FPGA Emulation Platform for OpenCL(TM)", 47))
		#else
		if (!strncmp(platform_name, "Intel(R) FPGA SDK for OpenCL(TM)", 32))
		#endif // EMULATION
		{
			cl_platform_index = i;
			break;
		}
		free(platform_name);
	}

	if (cl_platform_index < 0)
	{
		fprintf(stderr, "ERROR - Couldn't find a supported OpenCL platform\n");
		exit(EXIT_FAILURE);
	}
	platform_id = p_platforms[cl_platform_index];
	

	/* 1.3: Choose OpenCL device */
	cl_uint cl_devices;
	OPENCL_CHECK( clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_ALL, 0, NULL, &cl_devices) );
	if (cl_devices <= 0)
	{
		fprintf(stderr, "ERROR - Couldn't find a supported OpenCL device\n");
		exit(EXIT_FAILURE);
	}

	cl_device_id *p_devices = (cl_device_id *)malloc(cl_devices * sizeof(cl_device_id));
	MALLOC_CHECK( p_devices );
	OPENCL_CHECK( clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_ACCELERATOR, cl_devices, p_devices, NULL) );
	device_id = p_devices[0];
	free(p_platforms);

	/* 1.4 Get and print device name */
	size_t device_name_size;
	OPENCL_CHECK( clGetDeviceInfo(device_id, CL_DEVICE_NAME, 0, NULL, &device_name_size) );
	char *device_name = (char *)malloc(sizeof(char) * device_name_size);
	MALLOC_CHECK( device_name );
	OPENCL_CHECK( clGetDeviceInfo(device_id, CL_DEVICE_NAME, device_name_size, device_name, NULL) );

	printf("\n ---------------- DEVICE INFORMATION ---------------- \n");
	printf(" PLATFORM: %s\n", platform_name);
	printf(" DEVICE: %s\n", device_name);
	printf("\n ---------------------------------------------------- \n");
	fflush(stdout);

	free(platform_name);
	free(device_name);

	/* 1.5: Create context */
	cl_context_properties context_properties[] = {
		CL_CONTEXT_PLATFORM,
		(cl_context_properties)platform_id,
		0
	};
	context = clCreateContext(context_properties, 1, &device_id, NULL, NULL, &err);
	OPENCL_CHECK_ERROR( err );

	/* 1.6: Load kernel file */
	FILE *kernel_file;
	char *kernel_path = (char *)malloc(512 * sizeof(char));
	MALLOC_CHECK( kernel_path );
	kernel_path[0] = '\0';
	strcat(kernel_path, "./process_frame");
	#if defined(PROFILING)
	strcat(kernel_path, "_profiling");
	#elif defined(EMULATION)
	strcat(kernel_path, "_emu");
	#endif // PROFILING || EMULATION
	strcat(kernel_path, ".aocx");
	if (!(kernel_file = fopen(kernel_path, "rb")))
	{
		fprintf(stderr, "ERROR - Couldn't find the kernel file at %s\n", kernel_path);
		exit(EXIT_FAILURE);
	}

	fseek(kernel_file, 0, SEEK_END);
	size_t kernel_length = ftell(kernel_file);
	byte *kernel_bin = (byte *)malloc((kernel_length + 1) * sizeof(byte));
	MALLOC_CHECK( kernel_bin );
	rewind(kernel_file);
	if (!fread(kernel_bin, sizeof(byte), kernel_length, kernel_file))
	{
		fprintf(stderr, "ERROR - Couldn't open the kernel file\n");
		exit(EXIT_FAILURE);
	}
	kernel_bin[kernel_length] = 0;

	/* 1.7: Create program */
	program = clCreateProgramWithBinary(context, 1, &device_id, (const size_t *)&kernel_length, (const byte **)&kernel_bin, NULL, &err);
	OPENCL_CHECK_ERROR( err );

	err = clBuildProgram(program, 1, &device_id, "", NULL, NULL);
	if (err == CL_BUILD_PROGRAM_FAILURE) {
		size_t log_size;
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
		char *log = (char *)malloc(log_size);
		MALLOC_CHECK( log );
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, log_size, log, NULL);
		printf("[OpenCL kernel build failure]\nLog dump:\n%s\n", log);
	}
	OPENCL_CHECK_ERROR( err );

	kernel_ME = clCreateKernel(program, "process_frame", &err);
	OPENCL_CHECK_ERROR( err );

	/* 1.8: Create queues and allocate device buffers */
	properties = 0;
	queue = clCreateCommandQueue(context, device_id, properties, &err);
	OPENCL_CHECK_ERROR( err );

	d_curr = clCreateBuffer(context, CL_MEM_READ_ONLY, (1920 + 30) * (1088 + 30) * sizeof(byte), NULL, &err);
	OPENCL_CHECK_ERROR( err );
	d_prev = clCreateBuffer(context, CL_MEM_READ_ONLY, (1920 + 30) * (1088 + 30) * sizeof(byte), NULL, &err);
	OPENCL_CHECK_ERROR( err );

	d_SAD_output = clCreateBuffer(context, CL_MEM_WRITE_ONLY, results_per_frame * sizeof(unsigned short), NULL, &err);
	OPENCL_CHECK_ERROR( err );
	d_x_vector_output = clCreateBuffer(context, CL_MEM_WRITE_ONLY, results_per_frame * sizeof(int8), NULL, &err);
	OPENCL_CHECK_ERROR( err );
	d_y_vector_output = clCreateBuffer(context, CL_MEM_WRITE_ONLY, results_per_frame * sizeof(int8), NULL, &err);
	OPENCL_CHECK_ERROR( err );

	/* 1.9: Wait until all OpenCL operations are executed */
	OPENCL_CHECK( clFinish(queue) );

	/* 2: Compute motion estimation over the video's frames as they're read. */
	cl_event sync_events[3];

	/* 2.1: Copy frames to device */
	OPENCL_CHECK( clEnqueueWriteBuffer(queue, d_curr, CL_FALSE, 0,
		(1920 + 30) * (1088 + 30) * sizeof(byte), (void *)curr, 0, NULL, sync_events) );
	OPENCL_CHECK( clFlush(queue) );
	OPENCL_CHECK( clWaitForEvents(1, sync_events ) );

	OPENCL_CHECK( clEnqueueWriteBuffer(queue, d_prev, CL_FALSE, 0,
		(1920 + 30) * (1088 + 30) * sizeof(byte), (void *)prev, 0, NULL, sync_events) );
	OPENCL_CHECK( clFlush(queue) );
	OPENCL_CHECK( clWaitForEvents(1, sync_events ) );

	/* 2.2: Set kernel args */
	OPENCL_CHECK( clSetKernelArg(kernel_ME, 2, 16 * 16 * sizeof(cl_uchar), NULL) );
	OPENCL_CHECK( clSetKernelArg(kernel_ME, 3, 46 * 46 * sizeof(cl_uchar), NULL) );
	OPENCL_CHECK( clSetKernelArg(kernel_ME, 4, sizeof(cl_mem), &d_SAD_output) );
	OPENCL_CHECK( clSetKernelArg(kernel_ME, 5, sizeof(cl_mem), &d_x_vector_output) );
	OPENCL_CHECK( clSetKernelArg(kernel_ME, 6, sizeof(cl_mem), &d_y_vector_output) );

	/* Main loop */
	double time1 = 0, time2;
	long checksum = 0;
	int frame_num = 0, _REP = REP;

	_REP += 100;
	while (_REP--)
	{
		if (_REP == REP) time1 = wtime();

		/* 2.2.1: Set kernel frame arguments */
		OPENCL_CHECK( clSetKernelArg(kernel_ME, 0, sizeof(cl_mem), &d_curr) );
		OPENCL_CHECK( clSetKernelArg(kernel_ME, 1, sizeof(cl_mem), &d_prev) );

		/* ME over the whole frame */		
		OPENCL_CHECK( clEnqueueTask(queue, kernel_ME, 0, NULL, NULL) );
		OPENCL_CHECK( clFlush(queue) );

		/* 2.4: Read results from device */
		OPENCL_CHECK( clEnqueueReadBuffer(queue, d_SAD_output, CL_FALSE, 0,
			results_per_frame * sizeof(unsigned short), (void *)&SADs[frame_num * results_per_frame],
			0, NULL, sync_events) );
		OPENCL_CHECK( clEnqueueReadBuffer(queue, d_x_vector_output, CL_FALSE, 0,
			results_per_frame * sizeof(int8), (void *)&x_vectors[frame_num * results_per_frame],
			0, NULL, &sync_events[1]) );
		OPENCL_CHECK( clEnqueueReadBuffer(queue, d_y_vector_output, CL_FALSE, 0,
			results_per_frame * sizeof(int8), (void *)&y_vectors[frame_num * results_per_frame],
			0, NULL, &sync_events[2]) );
		OPENCL_CHECK( clFlush(queue) );
		OPENCL_CHECK( clWaitForEvents(3, sync_events) );

		/* 2.5: Swap frames */
		tmp = d_curr;
		d_curr = d_prev;
		d_prev = tmp;

		frame_num = (frame_num + 1) % 2;
	}

	time2 = wtime();

	/* 3: Report results */

	/* 3.1: Compute checksum */
	for (int i = 0; i < n_results; ++i)
	{
		checksum += SADs[i];
	}

	/* 3.2: Report results */
	printf("\n=== FPGA RESULT ===\n");
	printf("Total checksum: %ld\n", checksum);
	printf("Number of different vectors computed: %d (%d vectors per frame)\n",
		n_results, results_per_frame);
	// Checksum for 1 repetition:  18292643
	// Checksum for 2 repetitions: 36431066
	printf("Execution time: %lf\n", time2 - time1);
	printf("Dumping motion vector inforimation to stderr...\n");

	frame_num = 0;
	do
	{
		for (unsigned short i = 0; i <= 1080 / 16; ++i)
			for (unsigned short j = 0; j < 1920 / 16; ++j)
				fprintf(stderr, "Frame %d: Macroblock (%d, %d) moves in (%d, %d) with SAD %d\n",
					frame_num, j, i,
					x_vectors[i * 1920 / 16 + j + frame_num * results_per_frame],
					y_vectors[i * 1920 / 16 + j + frame_num * results_per_frame],
					SADs[i * 1920 / 16 + j + frame_num * results_per_frame]);
		frame_num++;
	} while (--REP % 2);

	/* 4: Free resources */
	free(curr);
	free(prev);
	free(SADs);
	free(x_vectors);
	free(y_vectors);

	OPENCL_CHECK( clReleaseMemObject(d_curr) );
	OPENCL_CHECK( clReleaseMemObject(d_prev) );
	OPENCL_CHECK( clReleaseMemObject(d_SAD_output) );
	OPENCL_CHECK( clReleaseMemObject(d_x_vector_output) );
	OPENCL_CHECK( clReleaseMemObject(d_y_vector_output) );

	OPENCL_CHECK( clReleaseKernel(kernel_ME) );
	OPENCL_CHECK( clReleaseProgram(program) );
	OPENCL_CHECK( clReleaseCommandQueue(queue) );
	OPENCL_CHECK( clReleaseContext(context) );

	return 0;
}
