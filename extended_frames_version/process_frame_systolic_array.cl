typedef unsigned char byte;
typedef char s8;
typedef unsigned short u16;
typedef short s16;

#define SA_WIDTH 46
#define SHIFT_INPUT 0
#define SHIFT_1BYTE 1
#define SHIFT_LINE 2

void SR(local byte * restrict macroblock, local byte * restrict search_area,
	byte shift, byte data_in)
{
	switch (shift)
	{
	case SHIFT_INPUT:
		#pragma unroll
		for (int k = 255; k > 0; --k) macroblock[k] = macroblock[k - 1];
		macroblock[0] = search_area[46 * SA_WIDTH - 1];
		#pragma unroll
		for (int k = 46 * SA_WIDTH - 1; k > 0; --k) search_area[k] = search_area[k - 1];
		search_area[0] = data_in;
		break;
	case SHIFT_1BYTE:
		#pragma unroll
		for (int k = 46 * SA_WIDTH - 1; k > 0; --k) search_area[k] = search_area[k - 1];
		break;
	case SHIFT_LINE:
		#pragma unroll
		for (int k = 46 * SA_WIDTH - 1; k >= 16; --k) search_area[k] = search_area[k - 16];
		break;
	default:
		break;
	}
}

u16 SAD_macroblock(local byte * restrict macroblock, local byte * restrict search_area)
{
	u16 sad = 0;
	s16 dif;
	const u16 SA_OFFSET = (30 * SA_WIDTH + 30);

	#pragma unroll
	for (s16 _ = 0; _ < 256; ++_)
	{
		s8 i = _ / 16, j = _ % 16;
		dif = macroblock[_] - search_area[i * SA_WIDTH + j + SA_OFFSET];
		sad += dif < 0 ? -dif : dif;
	}

	return sad;
}

void motion_estimation(global byte * restrict curr, global byte * restrict ref,
	local byte * restrict macroblock, local byte * restrict search_area,
	u16 x, u16 y, u16 pels,
	global u16 * restrict SAD,
	global s8 * restrict vector_x, global s8 * restrict vector_y)
{
	u16 sad, best_sad = USHRT_MAX;
	s8 vx, vy; // Horizontal and vertical displacements

	/* Load macroblock */
	for (u16 _ = 0; _ < 256; ++_)
	{
		byte i = _ / 16, j = _ % 16;
		SR(macroblock, search_area, SHIFT_INPUT, curr[((y + i) * pels) + x + j]);
	}
	/* Load search_area */
	for (u16 _ = 0; _ < 46 * SA_WIDTH - 1; ++_)
	{
		byte i = _ / SA_WIDTH - 15, j = _ % SA_WIDTH - 15;
		SR(macroblock, search_area, SHIFT_INPUT, ref[((y + i) * pels) + x + j]);
	}

	for (s8 i = -15; i <= 15; ++i)
	{
		for (s8 j = -15; j <= 15; ++j)
		{
			sad = SAD_macroblock(macroblock, search_area);
			SR(macroblock, search_area, j < 15 ? SHIFT_1BYTE : SHIFT_LINE, 0);

			if (sad < best_sad)
			{
				best_sad = sad; // We take the best (closest) match
				vx = j;
				vy = i;
			}
		}
	}

	*SAD = best_sad;
	*vector_x = vx;
	*vector_y = vy;
}

__attribute__((max_global_work_dim(0)))
kernel void process_frame(global byte * restrict curr, global byte * restrict ref,
	__attribute__((local_mem_size(256))) local byte * restrict macroblock,
	__attribute__((local_mem_size(4096))) local byte * restrict search_area,
	global u16 * restrict SADs,
	global s8 * restrict x_vectors, global s8 * restrict y_vectors)
{
	u16 _ = 0;

	for (u16 i = 15; i < 1088 + 15; i += 16)
	{
		for (u16 j = 15; j < 1920 + 15; j += 16)
		{
			motion_estimation(curr, ref,
				macroblock, search_area,
				j, i, 1920 + 30,
				&SADs[_], &x_vectors[_], &y_vectors[_]);
			_++;
		}
	}
}
