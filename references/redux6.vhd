library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity redux6 is
  Port ( ina, inb, inc, ind, ine, inf : in std_logic;
  suma : out std_logic_vector(2 downto 0));
end redux6;

architecture rtl of redux6 is
signal inp : std_logic_vector(5 downto 0);  
begin

inp <= ina & inb & inc & ind & ine & inf;

process(inp)
begin
case inp is
when "000000" => suma <= "000";
when "000001" => suma <= "001";
when "000010" => suma <= "001";
when "000011" => suma <= "010";
when "000100" => suma <= "001";
when "000101" => suma <= "010";
when "000110" => suma <= "010";
when "000111" => suma <= "011";
when "001000" => suma <= "001";
when "001001" => suma <= "010";
when "001010" => suma <= "010";
when "001011" => suma <= "011";
when "001100" => suma <= "010";
when "001101" => suma <= "011";
when "001110" => suma <= "011";
when "001111" => suma <= "100";
when "010000" => suma <= "001";
when "010001" => suma <= "010";
when "010010" => suma <= "010";
when "010011" => suma <= "011";
when "010100" => suma <= "010";
when "010101" => suma <= "011";
when "010110" => suma <= "011";
when "010111" => suma <= "100";
when "011000" => suma <= "010";
when "011001" => suma <= "011";
when "011010" => suma <= "011";
when "011011" => suma <= "100";
when "011100" => suma <= "011";
when "011101" => suma <= "100";
when "011110" => suma <= "100";
when "011111" => suma <= "101";
when "100000" => suma <= "001";
when "100001" => suma <= "010";
when "100010" => suma <= "010";
when "100011" => suma <= "011";
when "100100" => suma <= "010";
when "100101" => suma <= "011";
when "100110" => suma <= "011";
when "100111" => suma <= "100";
when "101000" => suma <= "010";
when "101001" => suma <= "011";
when "101010" => suma <= "011";
when "101011" => suma <= "100";
when "101100" => suma <= "011";
when "101101" => suma <= "100";
when "101110" => suma <= "100";
when "101111" => suma <= "101";
when "110000" => suma <= "010";
when "110001" => suma <= "011";
when "110010" => suma <= "011";
when "110011" => suma <= "100";
when "110100" => suma <= "011";
when "110101" => suma <= "100";
when "110110" => suma <= "100";
when "110111" => suma <= "101";
when "111000" => suma <= "011";
when "111001" => suma <= "100";
when "111010" => suma <= "100";
when "111011" => suma <= "101";
when "111100" => suma <= "100";
when "111101" => suma <= "101";
when "111110" => suma <= "101";
when others => suma <= "110";
end case;

end process;

end rtl;
