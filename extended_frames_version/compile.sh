#/bin/bash

#time aoc -v -report SAD_macroblock.cl -o SAD_macroblock.aocx

#time aoc -v -report -board-package=/glob/development-tools/versions/oneapi/2022.1.2/oneapi/compiler/2022.0.2/linux/lib/oclfpga/board/intel_s10sx_pac SAD_macroblock.cl -o SAD_macroblock.aocx
time aoc -v -report -march=emulator process_frame.cl -o process_frame_emu.aocx
time aoc -v -report -board-package=/glob/development-tools/versions/oneapi/2022.1.2/oneapi/intelfpgadpcpp/2022.1.0/board/intel_s10sx_pac process_frame.cl -o process_frame.aocx
#time aoc -v -report  -parallel=8 -fast-compile -no-hardware-kernel-invocation-queue -g0 -board-package=/glob/development-tools/versions/oneapi/2022.1.2/oneapi/intelfpgadpcpp/2022.1.0/board/intel_s10sx_pac process_frame.aocr -o process_frame.aocx
