typedef unsigned char byte;
typedef char s8;
typedef unsigned short u16;
typedef short s16;

#define REF_WIDTH 46

#define N  (-2 * REF_WIDTH)
#define S  ( 2 * REF_WIDTH)
#define E  ( 2)
#define W  (-2)
#define NE (-1 * REF_WIDTH + 1)
#define NW (-1 * REF_WIDTH - 1)
#define SE ( 1 * REF_WIDTH + 1)
#define SW ( 1 * REF_WIDTH - 1)

u16 SAD_macroblock(local byte * restrict search_area, local byte * restrict macroblock)
{
	u16 sad = 0;

	#pragma unroll
	for (s16 _ = 0; _ < 256; ++_)
	{
		s8 i = _ / 16, j = _ % 16;
		sad += abs(search_area[i * REF_WIDTH + j] - macroblock[_]);
	}

	return sad;
}

void load_frame(local byte out[(1920 + 30) * (1088 + 30)], global byte * restrict in)
{
	for (int i = 0; i < (1920 + 30) * (1088 + 30); ++i)
		 out[i] = in[i];
}

void load_macroblock(local byte * restrict out, local byte cur_frame[(1920 + 30) * (1088 + 30)],
	u16 x, u16 y, u16 lx)
{
	local byte * restrict in = cur_frame + (y * lx) + x;

	for (s16 _ = 0; _ < 256; ++_)
	{
		s8 i = _ / 16, j = _ % 16;
		out[_] = in[i * lx + j];
	}
}

void load_ref_block(local byte * restrict out, local byte ref_frame[(1920 + 30) * (1088 + 30)],
	u16 x, u16 y, u16 lx)
{
	local byte * restrict in = ref_frame + (y * lx) + x;

	for (s16 _ = 0; _ < 46 * REF_WIDTH; ++_)
	{
		s8 i = _ / REF_WIDTH, j = _ % REF_WIDTH;
		out[i * REF_WIDTH + j] = in[i * lx + j];
	}
}

void motion_estimation(local byte * restrict macroblock, local byte * restrict ref_block,
	global u16 * restrict SAD,
	global s8 * restrict vector_x, global s8 * restrict vector_y)
{
	local byte * restrict search_area = ref_block;

	/* Get SAD for center candidate (first step) */
	s16 offset, prev_offset, best_offset;
	u16 sad, best_sad;
	offset = prev_offset = best_offset = 15 * REF_WIDTH + 15; // Center offset
	sad = best_sad = SAD_macroblock(search_area + offset, macroblock);
	s8 vx = 0, vy = 0; // Horizontal and vertical displacements

	s8 search_pattern[8] = { N, NW, NE, W, E, SW, SE, S };
	byte search_pattern_s = 8;

	/* Large diamond search pattern (second step) */
	byte min_center = 0;
	while (!min_center)
	{
		min_center = 1;

		for (byte i = 0; i < search_pattern_s; ++i)
		{
			/* Check offset is within valid frame limits */
			if (search_pattern[i] == N && vy <= -14) continue;
			if (search_pattern[i] == S && vy >=  14) continue;
			if (search_pattern[i] == E && vx >=  14) continue;
			if (search_pattern[i] == W && vx <= -14) continue;
			if ((search_pattern[i] == NW || offset == NE) && vy == -15) continue;
			if ((search_pattern[i] == SW || offset == SE) && vy ==  15) continue;
			if ((search_pattern[i] == SE || offset == NE) && vx ==  15) continue;
			if ((search_pattern[i] == SW || offset == NW) && vx == -15) continue;

			offset = prev_offset + search_pattern[i];
			sad = SAD_macroblock(search_area + offset, macroblock);

			if (sad < best_sad)
			{
				best_sad = sad;
				best_offset = offset;
				min_center = 0;
			}
		}

		switch (best_offset - prev_offset)
		{
			case N:
				search_pattern[0] = N;
				search_pattern[1] = NW;
				search_pattern[2] = NE;
				search_pattern[3] = W;
				search_pattern[4] = E;
				search_pattern[5] = 0;
				search_pattern[6] = 0;
				search_pattern[7] = 0;
				search_pattern_s = 5;
				vy -= 2;
				break;
			case S:
				search_pattern[0] = W;
				search_pattern[1] = E;
				search_pattern[2] = SW;
				search_pattern[3] = SE;
				search_pattern[4] = S;
				search_pattern[5] = 0;
				search_pattern[6] = 0;
				search_pattern[7] = 0;
				search_pattern_s = 5;
				vy += 2;
				break;
			case E:
				search_pattern[0] = N;
				search_pattern[1] = NE;
				search_pattern[2] = E;
				search_pattern[3] = SE;
				search_pattern[4] = S;
				search_pattern[5] = 0;
				search_pattern[6] = 0;
				search_pattern[7] = 0;
				search_pattern_s = 5;
				vx += 2;
				break;
			case W:
				search_pattern[0] = N;
				search_pattern[1] = NW;
				search_pattern[2] = W;
				search_pattern[3] = SW;
				search_pattern[4] = S;
				search_pattern[5] = 0;
				search_pattern[6] = 0;
				search_pattern[7] = 0;
				search_pattern_s = 5;
				vx -= 2;
				break;
			case NW:
				search_pattern[0] = N;
				search_pattern[1] = NW;
				search_pattern[2] = W;
				search_pattern[3] = 0;
				search_pattern[4] = 0;
				search_pattern[5] = 0;
				search_pattern[6] = 0;
				search_pattern[7] = 0;
				search_pattern_s = 3;
				vy -= 1;
				vx -= 1;
				break;
			case SW:
				search_pattern[0] = W;
				search_pattern[1] = SW;
				search_pattern[2] = S;
				search_pattern[3] = 0;
				search_pattern[4] = 0;
				search_pattern[5] = 0;
				search_pattern[6] = 0;
				search_pattern[7] = 0;
				search_pattern_s = 3;
				vy += 1;
				vx -= 1;
				break;
			case NE:
				search_pattern[0] = N;
				search_pattern[1] = NW;
				search_pattern[2] = E;
				search_pattern[3] = 0;
				search_pattern[4] = 0;
				search_pattern[5] = 0;
				search_pattern[6] = 0;
				search_pattern[7] = 0;
				search_pattern_s = 3;
				vy -= 1;
				vx += 1;
				break;
			case SE:
				search_pattern[0] = E;
				search_pattern[1] = SE;
				search_pattern[2] = S;
				search_pattern[3] = 0;
				search_pattern[4] = 0;
				search_pattern[5] = 0;
				search_pattern[6] = 0;
				search_pattern[7] = 0;
				search_pattern_s = 3;
				vy += 1;
				vx += 1;
				break;
			default:
				search_pattern[0] = -1 * REF_WIDTH;
				search_pattern[1] = -1;
				search_pattern[2] = 1;
				search_pattern[3] = 1 * REF_WIDTH;
				search_pattern[4] = 0;
				search_pattern[5] = 0;
				search_pattern[6] = 0;
				search_pattern[7] = 0;
				search_pattern_s = 4;
				break;
		}
		prev_offset = best_offset;
	}

	/* Small diamond search pattern (last step) */
	for (byte i = 0; i < search_pattern_s; ++i)
	{
		/* Check offset is within valid frame limits */
		if ((search_pattern[i] == -1 * REF_WIDTH) && vy == -15) continue;
		if ((search_pattern[i] ==  1 * REF_WIDTH) && vy ==  15) continue;
		if ((search_pattern[i] ==  1) && vx ==  15) continue;
		if ((search_pattern[i] == -1) && vx == -15) continue;

		offset = prev_offset + search_pattern[i];
		sad = SAD_macroblock(search_area + offset, macroblock);

		if (sad < best_sad)
		{
			best_sad = sad;
			best_offset = offset;
		}
	}

	switch (best_offset - prev_offset)
	{
		case (-1 * REF_WIDTH):
			vy -= 1;
			break;
		case ( 1 * REF_WIDTH):
			vy += 1;
			break;
		case ( 1):
			vx += 1;
			break;
		case (-1):
			vx -= 1;
			break;
		default:
			break;
	}

	*SAD = best_sad;
	*vector_x = vx;
	*vector_y = vy;
}

__attribute__((max_global_work_dim(0)))
kernel void process_frame(global byte * restrict curr, global byte * restrict prev,
	__attribute__((local_mem_size(256))) local byte * restrict macroblock,
	__attribute__((local_mem_size(4096))) local byte * restrict ref_block,
	global u16 * restrict SADs,
	global s8 * restrict x_vectors, global s8 * restrict y_vectors)
{
	local byte cur_frame[(1920 + 30) * (1088 + 30)];
	local byte ref_frame[(1920 + 30) * (1088 + 30)];

	load_frame(cur_frame, curr);
	load_frame(ref_frame, prev);

	u16 _ = 0;

	for (u16 i = 15; i < 1088 + 15; i += 16)
	{
		for (u16 j = 15; j < 1920 + 15; j += 16)
		{

			//#pragma loop_fuse independent
			{
			// Load area macroblock
			load_macroblock(macroblock, cur_frame, j, i, 1920 + 30);

			// Load area ref_block
			load_ref_block(ref_block, ref_frame,
				j - 15, i - 15, // Pixel from prev from where to start to copy
				1920 + 30);
			}

			motion_estimation(macroblock, ref_block,
				&SADs[_], &x_vectors[_], &y_vectors[_]);
			_++;
		}
	}
}
